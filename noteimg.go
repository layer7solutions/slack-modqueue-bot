package main

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io"
	"regexp"
	"strings"

	"github.com/gofiber/fiber"
)

//const noteSVG = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#%s" width="48px" height="48px"><path d="M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18zM18 14H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z"/><path d="M0 0h24v24H0z" fill="none"/></svg>`
const notePNGData = `iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAARVBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD////cVMzlAAAAFXRSTlMAGxwjJneAqKmqq63s7fHy+vv8/f6/uIZyAAAAAWJLR0QWfNGoGQAAAIBJREFUSMfd1skOgCAMRdGi1gFRnPj/X3WhIW4MfZEY4K57kgIbiEqp0tYFsoN6zE9OkKk90E5U54GVAeOBbN4tOYC3Z0oXrOihIwB4pQRBoO07CKyUBYh9S/uPYEbBKAOHB40BAal+xkAgRsEtAHAJBBCjgBgFxCggRgG1pfwyTqpLUjamfjFDAAAAAElFTkSuQmCC`
const noteURL = "https://dtg-slack.layer7.solutions/noteImg"

var hexReg = regexp.MustCompile(`^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$`)

func notePNG() io.Reader {
	return base64.NewDecoder(base64.StdEncoding, strings.NewReader(notePNGData))
}

func parseHexColor(s string) (c color.RGBA, err error) {
	c.A = 0xff
	switch len(s) {
	case 7:
		_, err = fmt.Sscanf(s, "#%02x%02x%02x", &c.R, &c.G, &c.B)
	case 4:
		_, err = fmt.Sscanf(s, "#%1x%1x%1x", &c.R, &c.G, &c.B)
		// Double the hex digits:
		c.R *= 17
		c.G *= 17
		c.B *= 17
	default:
		err = fmt.Errorf("invalid length, must be 7 or 4")
	}
	return
}

func handleNoteImg(c *fiber.Ctx) {
	l := log.WithField("prefix", "noteImg")

	color := c.Query("c")
	if len(color) == 0 || !hexReg.MatchString(color) {
		l.Warn("unknown/no color")
		c.Status(fiber.StatusNotFound)
		return
	}
	l.Debugf("requested image in color %s", color)
	newcolor, err := parseHexColor("#" + color)
	if err != nil {
		l.WithError(err).Error("couldn't parse hex color")
		c.Status(fiber.StatusBadRequest)
		return
	}

	img, err := png.Decode(notePNG())
	if err != nil {
		l.WithError(err).Error("couldn't decode png")
		c.Status(fiber.StatusInternalServerError)
		return
	}

	bounds := img.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y
	newImg := image.NewNRGBA(bounds)
	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			oldColor := img.At(x, y)
			_, _, _, a := oldColor.RGBA()
			if a == 0 {
				continue
			}
			c := newcolor
			c.A = uint8(a / 0x101)
			newImg.Set(x, y, c)
		}
	}

	c.Set("Content-Type", "image/png")
	c.Set("Cache-Control", "public, max-age=31536000")
	var buf bytes.Buffer
	if err := png.Encode(&buf, newImg); err != nil {
		l.WithError(err).Error("couldn't encode png")
		c.Status(fiber.StatusInternalServerError)
		return
	}
	c.SendBytes(buf.Bytes())
}
