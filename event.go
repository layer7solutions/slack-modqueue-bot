package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/gofiber/fiber"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	miramodels "github.com/ttgmpsn/mira/models"
)

func handleEvent(c *fiber.Ctx) {
	l := log.WithField("prefix", "event")

	eventsAPIEvent, err := slackevents.ParseEvent(json.RawMessage(c.Body()), slackevents.OptionVerifyToken(&slackevents.TokenComparator{VerificationToken: config.Slack.VerificationToken}))
	if err != nil {
		l.WithError(err).Error("could not parse event")
		c.Status(fiber.StatusInternalServerError)
	}

	if eventsAPIEvent.Type == slackevents.URLVerification {
		var r *slackevents.ChallengeResponse
		err := json.Unmarshal([]byte(c.Body()), &r)
		if err != nil {
			l.WithError(err).Error("could not read challenge")
			c.Status(fiber.StatusInternalServerError)
		}
		c.Set("Content-Type", fiber.MIMETextPlain)
		c.Send(r.Challenge)
		return
	}
	if eventsAPIEvent.Type == slackevents.CallbackEvent {
		innerEvent := eventsAPIEvent.InnerEvent
		switch ev := innerEvent.Data.(type) {
		case *slackevents.MessageEvent:
			l = l.WithField("event", "message").WithField("UID", ev.User)
			l.Debugf("Got %v", ev)
			if len(ev.Text) != 0 && ev.ChannelType == "channel" {
				logChannelMessage(ev)
			}
			if len(ev.Text) == 0 || ev.ChannelType != "im" || ev.User == slackInfo.UserID || ev.User == "USLACKBOT" {
				return
			}

			if ev.Text == "SNOORESET" {
				l.Info("got SNOORESET")
				addNoteStates.Del(ev.User)
			}
			if _, okUser := slackUsers.GetName(ev.User); !okUser {
				l.Warn("unknown user")
				slackAPI.PostMessage(ev.Channel, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
					slack.MsgOptionAttachments(slack.Attachment{
						Color:    "danger",
						Fallback: "You are not signed up. Please use /modsignup",
						Title:    "You are not signed up",
						Text:     "Please sign up using the /modsignup command, and try again.",
						Footer:   "If this error keeps occuring, complain to ttgmpsn",
					}),
				)
				return
			}

			// check if state exists
			state, okState := addNoteStates.Get(ev.User)
			// state.NoteType is 0 if the user doesn't cancel the
			// "select category" prompt, but goes on to enter a new username
			// codename: "squeaks anti-fail mechanic"
			if !okState || state.NoteType == 0 {
				// Delete Category select
				if okState && state.SlackTS != "" {
					slackAPI.DeleteMessage(state.SlackCID, state.SlackTS)
				}
				go addNoteStart(ev.User, ev.Channel, strings.Split(ev.Text, " ")[0])
				return
			}
			go addNoteText(ev.User, ev.Text, state)
		case *slackevents.AppHomeOpenedEvent:
			updateAppHome(ev.User)
		default:
			l.Warnf("received unknown event %T", ev)
		}
	}
}

func updateAppHome(userID string) {
	l := log.WithField("prefix", "updateAppHome").WithField("UID", userID)
	var blocks []slack.Block

	uReddit, ok := slackUsers.GetReddit(userID)
	var redditUser string
	if ok {
		l.Debug("reddit exists, checking if login works")
		rMeObj, err := uReddit.Me().Info()
		if err != nil {
			l.WithError(err).Warn("reddit login failed, does not work anymore")
			slackUsers.DeleteReddit(userID)
		}
		rMe, _ := rMeObj.(*miramodels.Me)
		l.Infof("Connected to Reddit as /u/%s", rMe.Name)
		redditUser = rMe.Name
	}
	_, okNotes := slackUsers.GetName(userID)
	if !okNotes || len(redditUser) == 0 {
		// user not logged in
		l.Debug("user not logged in")
		blocks = []slack.Block{slack.NewSectionBlock(
			slack.NewTextBlockObject("mrkdwn", "Hello! Unfortunately this page only works when you're logged in. Press the button to start now!", false, false),
			nil,
			slack.NewAccessory(slack.NewButtonBlockElement("signup_action", "open", slack.NewTextBlockObject("plain_text", ":lock: Sign In", true, false))),
		)}
	} else {
		blocks = []slack.Block{
			slack.NewSectionBlock(
				slack.NewTextBlockObject("mrkdwn", fmt.Sprintf("Hello /u/%s!", redditUser), false, false),
				nil,
				slack.NewAccessory(slack.NewButtonBlockElement("signup_action", "open", slack.NewTextBlockObject("plain_text", ":unlock: Manage Login", true, false))),
			),
			slack.NewDividerBlock(),
			slack.NewSectionBlock(slack.NewTextBlockObject("mrkdwn", ":notebook_with_decorative_cover: To view and add User notes, please paste a link or username in <slack://app?team=T04C29D3W&id=A9T510SUA&tab=messages|the \"Messages\" tab>.", false, false), nil, nil),
			slack.NewDividerBlock(),
			slack.NewSectionBlock(slack.NewTextBlockObject("mrkdwn", ":male-factory-worker: Tasks:", false, false), nil, nil),
			slack.NewActionBlock("home-actions",
				slack.NewButtonBlockElement("dtgbot-reset", "", slack.NewTextBlockObject("plain_text", ":sunrise: Post/Update Reset Thread", true, false)),
				slack.NewButtonBlockElement("dtgbot-ff", "", slack.NewTextBlockObject("plain_text", ":spiral_note_pad: Post Focused Feedback", true, false)),
				slack.NewButtonBlockElement("modremove", "", slack.NewTextBlockObject("plain_text", ":x: Remove Post/Comment", true, false)),
				slack.NewButtonBlockElement("dtgbot-twab", "", slack.NewTextBlockObject("plain_text", ":arrows_counterclockwise: Post/Update TWAB", true, false)),
				slack.NewButtonBlockElement("dtgbot-replies", "", slack.NewTextBlockObject("plain_text", ":male-teacher: Rescan Bungie Replies", true, false)),
				slack.NewButtonBlockElement("dtgbot-banner", "", slack.NewTextBlockObject("plain_text", ":frame_with_picture: Change Subreddit Banner", true, false)),
			),
		}
	}
	_, err := slackAPI.PublishView(userID, slack.HomeTabViewRequest{
		Type:   slack.VTHomeTab,
		Blocks: slack.Blocks{BlockSet: blocks},
	}, "")
	if err != nil {
		l.WithError(err).Error("could not publish home view")
		return
	}
}
