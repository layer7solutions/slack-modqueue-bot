package main

import (
	"fmt"
	"time"

	"github.com/slack-go/slack"
)

var postedQueue = make(map[string]string)

const queueLimit = 100

func buildQueue(subreddit string) {
	l := log.WithField("prefix", "buildQueue")

	if footer, ok := postedQueue["FOOTER"]; ok {
		slackAPI.UpdateMessage(queueChannel, footer, slack.MsgOptionBlocks(
			slack.NewSectionBlock(
				slack.NewTextBlockObject("mrkdwn", ":timer_clock: Updating...", false, false),
				nil, nil)),
		)
	}

	sub := reddit.Subreddit(subreddit)
	l.Debugf("Getting ModQueue")
	queue, err := sub.ModQueue(queueLimit)
	if err != nil {
		l.WithError(err).Errorf("Could not get modqueue")
		return
	}
	things := make(map[string]bool)
	new := false
	for k := range queue {
		v := queue[len(queue)-1-k]
		thingID := string(v.GetID())
		l.Debugf("processed item %s", thingID)
		if _, ok := postedQueue[thingID]; !ok {
			postMessage(queueChannel, thingID, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(), slack.MsgOptionDisableMediaUnfurl(), slack.MsgOptionBlocks(submissionToMessage(v)...))
			l.Debug("posting")
			new = true
		} else {
			l.Debug("skipping, exists")
			// do nothing
		}
		things[thingID] = true
	}
	for thingID, ts := range postedQueue {
		if _, ok := things[thingID]; !ok {
			if thingID == "FOOTER" {
				continue
			}
			l.Debugf("%s not in queue anymore, deleting", thingID)
			slackAPI.DeleteMessage(queueChannel, ts)
			delete(postedQueue, thingID)
		}
	}
	l.Debugf("done updating modqueue")

	db.Exec(`INSERT INTO "dtg_queuesize" ("size") VALUES (?);`, len(queue))

	if footer, ok := postedQueue["FOOTER"]; ok {
		if new {
			slackAPI.DeleteMessage(queueChannel, footer)
			delete(things, "FOOTER")
		} else {
			l.Debugf("no new items, just updating footer")
			slackAPI.UpdateMessage(queueChannel, footer, slack.MsgOptionBlocks(queueFooter(len(queue))))
			return
		}
	}

	postMessage(queueChannel, "FOOTER", slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionBlocks(queueFooter(len(queue))))
}

// clearChannel deletes all own messages from a channel
func clearChannel() {
	l := log.WithField("prefix", "clearChannel")
	l.Infof("Deleting old messages from queue channel")
	msgs, err := slackAPI.GetConversationHistory(&slack.GetConversationHistoryParameters{
		ChannelID: queueChannel,
		Limit:     500,
	})
	if err != nil {
		l.WithError(err).Errorf("could not get converstaion history")
		return
	}
	delQueue := make(chan string)
	go func() {
		for msgTS := range delQueue {
			for {
				slackAPI.DeleteMessage(queueChannel, msgTS)
				if err == nil {
					break
				}
				if rlErr, ok := err.(*slack.RateLimitedError); ok && rlErr.Retryable() {
					l.Infof("Hit Rate Limit Error, will try again in %d", rlErr.RetryAfter)
					time.Sleep(rlErr.RetryAfter)
					continue // Try again next loop
				}
				l.WithError(err).Warnf("could not delete a message with TS %s", msgTS)
				break
			}
		}
	}()
	for _, msg := range msgs.Messages {
		if msg.User != slackInfo.UserID || msg.SubType == "group_join" {
			continue
		}
		delQueue <- msg.Timestamp
	}
	close(delQueue)
	postedQueue = make(map[string]string)
	l.Debug("Done deleting old messages")
}

func queueFooter(sum int) *slack.SectionBlock {
	var accessory slack.BlockElement
	if sum > 0 {
		overflow := slack.NewOverflowBlockElement(
			"queueadmin",
			slack.NewOptionBlockObject("rebuild", slack.NewTextBlockObject("plain_text", "Rebuild Queue", false, false), nil),
		)
		overflow.Confirm = slack.NewConfirmationBlockObject(
			slack.NewTextBlockObject("plain_text", "Confirm Action", false, false),
			slack.NewTextBlockObject("mrkdwn", "Are you sure you want to rebuild the queue? For long queues this might take some time due to Slack Rate Limiting.\nPleae only use this option when there are issues.", false, false),
			slack.NewTextBlockObject("plain_text", "Yes", false, false),
			slack.NewTextBlockObject("plain_text", "No", false, false),
		)
		accessory = overflow
	} else {
		accessory = slack.NewImageBlockElement("https://b.thumbs.redditmedia.com/7uBqTsML9Y-sksmfvCrKrHvoUANP6ZCOw9UOVIV6aXQ.png", "the queue is clean! kitteh is pleased")
	}
	summary := slack.NewSectionBlock(
		slack.NewTextBlockObject("mrkdwn",
			fmt.Sprintf(
				":burrito: %d Items | Last Refreshed <!date^%d^{date_num} {time_secs}|%d>",
				sum, time.Now().Unix(), time.Now().Unix(),
			),
			false, false),
		nil,
		slack.NewAccessory(accessory),
	)

	return summary
}
