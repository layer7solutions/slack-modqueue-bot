package main

import (
	"fmt"
	"time"

	"github.com/slack-go/slack"
	sn "github.com/ttgmpsn/go-snoonotes"
	miramodels "github.com/ttgmpsn/mira/models"
)

func submissionToMessage(s miramodels.Submission) []slack.Block {
	l := log.WithField("prefix", "submissionToMessage").WithField("thingID", s.GetID())
	ret := []slack.Block{slack.NewDividerBlock()}

	var headerText, titleText string
	if s.GetID().Type() == miramodels.KComment {
		headerText = ":speech_balloon: *Comment*"
		c, err := reddit.Comment(string(s.GetID())).GetParentPost()
		if err != nil {
			l.WithError(err).Warnf("could not get parent post for comment")
		}
		post, err := reddit.SubmissionInfoID(c)
		if err != nil {
			l.WithError(err).Warnf("could not get parent post '%s' for comment", c)
			titleText = fmt.Sprintf("Comment on <%s?context=1000|(unknown)>", s.GetURL())
		} else {
			titleText = fmt.Sprintf("Comment on <%s?context=1000|%s>", s.GetURL(), post.GetTitle())
		}
	} else {
		post, _ := s.(*miramodels.Post)
		headerText = ":spiral_note_pad: *Post*"
		titleText = fmt.Sprintf("[%s] <%s|%s>", post.LinkFlairText, s.GetURL(), s.GetTitle())
	}

	var status string
	if s.GetBanned().Mod == "AutoModerator" {
		status = "AutoMod filtered"
	} else {
		status = fmt.Sprintf("%dx reported", s.GetReports().Num)
	}

	header := slack.NewContextBlock("",
		slack.NewTextBlockObject("mrkdwn",
			fmt.Sprintf(
				"%s by <https://www.reddit.com/user/%s|/u/%s> | <!date^%d^{date_num} {time_secs}|%s> | Status: %s",
				headerText, s.GetAuthor(), s.GetAuthor(), s.CreatedAt().Unix(), s.CreatedAt().Format(time.UnixDate), status,
			),
			false, false),
	)
	ret = append(ret, header)

	linkOption := slack.NewOptionBlockObject("open-reddit", slack.NewTextBlockObject("plain_text", "Show on Reddit", false, false), nil)
	linkOption.URL = fmt.Sprintf("%s?context=1000", s.GetURL())
	title := slack.NewSectionBlock(
		slack.NewTextBlockObject("mrkdwn",
			titleText,
			false, false),
		nil,
		slack.NewAccessory(slack.NewOverflowBlockElement(
			"submission_actions",
			linkOption,
		)),
	)
	ret = append(ret, title)

	comment := slack.NewSectionBlock(
		slack.NewTextBlockObject("plain_text", trimString(s.GetBody(), 500), false, false),
		nil, nil,
	)
	ret = append(ret, comment)

	detailsText := ""
	if s.GetBanned().Mod == "AutoModerator" {
		reason := getAutomodRemoval(s.GetSubreddit(), s.GetID())
		if len(reason) == 0 {
			reason = "_(unknown)_"
		}
		detailsText += fmt.Sprintf("- Automod Reason: %s\n", reason)
	}
	for _, r := range s.GetReports().Mod {
		if len(r.Reason) == 0 {
			r.Reason = "_(empty)_"
		}
		detailsText += fmt.Sprintf("- *%s*: %s\n", insertZWS(r.Mod), r.Reason)
	}
	for _, r := range s.GetReports().User {
		if len(r.Reason) == 0 {
			r.Reason = "_(empty)_"
		}
		detailsText += fmt.Sprintf("- %dx: %s\n", r.Count, r.Reason)
	}
	if len(detailsText) > 0 {
		detailsText = fmt.Sprintf("*Reports*:\n%s", detailsText)
	}

	if len(s.GetApproved().Mod) > 0 {
		if len(detailsText) > 0 {
			detailsText += "\n"
		}
		detailsText += fmt.Sprintf("*Approved by* /u/%s on <!date^%d^{date_num} {time_secs}|%s>", insertZWS(s.GetApproved().Mod), s.GetApproved().At.Unix(), s.GetApproved().At.Format(time.UnixDate))
	}

	if len(detailsText) > 0 {
		reports := slack.NewSectionBlock(
			slack.NewTextBlockObject("mrkdwn", detailsText, false, false),
			nil, nil,
		)
		ret = append(ret, reports)
	}

	okButton := slack.NewButtonBlockElement("action_approve", string(s.GetID()), slack.NewTextBlockObject("plain_text", ":heavy_check_mark: Approve", true, false))
	okButton.WithStyle(slack.StylePrimary)
	noButton := slack.NewButtonBlockElement("action_remove", string(s.GetID()), slack.NewTextBlockObject("plain_text", ":heavy_multiplication_x: Further Actions...", true, false))
	noButton.WithStyle(slack.StyleDanger)
	actions := slack.NewActionBlock("",
		okButton, noButton,
	)
	ret = append(ret, actions)

	return append(ret, slack.NewDividerBlock())
}

func submissionToModal(s miramodels.Submission, userID string) []slack.Block {
	l := log.WithField("prefix", "submissionToModal").WithField("thingID", s.GetID())
	ret := []slack.Block{}

	var headerText, titleText string
	if s.GetID().Type() == miramodels.KComment {
		headerText = ":speech_balloon: *Comment*"
		c, err := reddit.Comment(string(s.GetID())).GetParentPost()
		if err != nil {
			l.WithError(err).Warnf("could not get parent post for comment")
		}
		post, err := reddit.SubmissionInfoID(c)
		if err != nil {
			l.WithError(err).Warnf("could not get parent post '%s' for comment", c)
			titleText = fmt.Sprintf("Comment on <%s?context=1000|(unknown)>", s.GetURL())
		} else {
			titleText = fmt.Sprintf("Comment on <%s?context=1000|%s>:\n", s.GetURL(), post.GetTitle())
		}
	} else {
		post, _ := s.(*miramodels.Post)
		headerText = ":spiral_note_pad: *Post*"
		titleText = fmt.Sprintf("[%s] <%s|%s>\n", post.LinkFlairText, s.GetURL(), s.GetTitle())
	}

	var status string
	if s.GetBanned().Mod == "AutoModerator" {
		status = "AutoMod filtered"
	} else {
		status = fmt.Sprintf("%dx reported", s.GetReports().Num)
	}

	header := slack.NewContextBlock("",
		slack.NewTextBlockObject("mrkdwn",
			fmt.Sprintf(
				"%s by <https://www.reddit.com/user/%s|/u/%s>",
				headerText, s.GetAuthor(), s.GetAuthor(),
			),
			false, false),
		slack.NewTextBlockObject("mrkdwn",
			fmt.Sprintf(
				"<!date^%d^{date_num} {time_secs}|%s>",
				s.CreatedAt().Unix(), s.CreatedAt().Format(time.UnixDate),
			),
			false, false),
		slack.NewTextBlockObject("mrkdwn",
			status,
			false, false),
	)
	ret = append(ret, header)

	detailsText := ""
	if s.GetBanned().Mod == "AutoModerator" {
		reason := getAutomodRemoval(s.GetSubreddit(), s.GetID())
		if len(reason) == 0 {
			reason = "_(unknown)_"
		}
		detailsText += fmt.Sprintf("- Automod Reason: %s\n", reason)
	}
	for _, r := range s.GetReports().Mod {
		if len(r.Reason) == 0 {
			r.Reason = "_(empty)_"
		}
		detailsText += fmt.Sprintf("- *%s*: %s\n", insertZWS(r.Mod), r.Reason)
	}
	for _, r := range s.GetReports().User {
		if len(r.Reason) == 0 {
			r.Reason = "_(empty)_"
		}
		detailsText += fmt.Sprintf("- %dx: %s\n", r.Count, r.Reason)
	}
	if len(detailsText) > 0 {
		detailsText = fmt.Sprintf("*Reports*:\n%s", detailsText)
	}

	if len(s.GetApproved().Mod) > 0 {
		if len(detailsText) > 0 {
			detailsText += "\n\n"
		}
		detailsText += fmt.Sprintf("*Approved by* /u/%s on <!date^%d^{date_num} {time_secs}|%s>", insertZWS(s.GetApproved().Mod), s.GetApproved().At.Unix(), s.GetApproved().At.Format(time.UnixDate))
	}
	if len(detailsText) > 0 {
		detailsText = "\n" + detailsText
	}

	linkOption := slack.NewOptionBlockObject("open-reddit", slack.NewTextBlockObject("plain_text", "Show on Reddit", false, false), nil)
	linkOption.URL = fmt.Sprintf("%s?context=1000", s.GetURL())
	preview := slack.NewSectionBlock(
		slack.NewTextBlockObject("mrkdwn", titleText+trimString(s.GetBody(), 100)+detailsText, false, false),
		nil,
		slack.NewAccessory(slack.NewOverflowBlockElement(
			"thing_actions",
			linkOption,
		)),
	)
	ret = append(ret, preview, slack.NewDividerBlock())

	user, _ := slackUsers.GetName(userID)
	types, err := sn.GetNoteTypeMap(user, config.Subreddit)
	if err != nil {
		l.WithError(err).Errorf("error getting note type map")
		return ret
	}
	notes, err := sn.Get(config.Subreddit, user, s.GetAuthor())
	if err != nil {
		l.WithError(err).Errorf("error getting notes")
		return ret
	}
	if notes != nil {
		note := (*notes)[len(*notes)-1]
		noteType, ok := types[note.NoteTypeID]
		if !ok {
			noteType = sn.SimpleNoteType{
				DisplayName: "UNKNOWN (TELL SOMEBODY!)",
				ColorCode:   "000",
			}
		}
		var t time.Time
		t, err = time.Parse(time.RFC3339Nano, note.TimeStamp)
		if err != nil {
			t = time.Now()
		}
		notes := slack.NewSectionBlock(
			slack.NewTextBlockObject("mrkdwn", fmt.Sprintf("*Last Note*: %s", note.Message), false, false),
			nil,
			slack.NewAccessory(slack.NewButtonBlockElement("note_action", "open-notes", slack.NewTextBlockObject("plain_text", fmt.Sprintf(":notebook_with_decorative_cover: Open all %d Notes", len(*notes)), true, false))),
		)
		context := slack.NewContextBlock(
			"",
			slack.NewImageBlockElement(fmt.Sprintf("%s?c=%s", noteURL, noteType.ColorCode), fmt.Sprintf("Note with color %s", noteType.ColorCode)),
			slack.NewTextBlockObject("plain_text", noteType.DisplayName, false, false),
			slack.NewTextBlockObject("plain_text", fmt.Sprintf(":male-police-officer: /u/%s", note.Submitter), false, false),
			slack.NewTextBlockObject("mrkdwn", fmt.Sprintf(":clock4: <!date^%d^{date_num} {time_secs}|%s>", t.Unix(), t.Format(time.UnixDate)), false, false),
		)
		ret = append(ret, notes, context, slack.NewDividerBlock())
	} else {
		ret = append(ret, slack.NewSectionBlock(
			slack.NewTextBlockObject("mrkdwn", "*Last Note*: _no previous notes_", false, false),
			nil, nil,
		), slack.NewDividerBlock())
	}

	boxes := []*slack.OptionBlockObject{}
	rc, err := getRemovalConfig(s.GetSubreddit())
	if err != nil {
		l.WithError(err).Error("could not get removal reasons")
		return ret
	}
	for _, r := range rc.Reasons {
		if s.GetID().Type() == miramodels.KComment && !r.RemoveComments {
			continue
		}
		if s.GetID().Type() == miramodels.KPost && !r.RemovePosts {
			continue
		}
		boxes = append(
			boxes,
			slack.NewOptionBlockObject(fmt.Sprintf("%d", r.ID), slack.NewTextBlockObject("plain_text", r.Title, false, false), nil),
		)
	}

	if len(boxes) > 10 {
		boxes = boxes[:10]
	}

	reasons := slack.NewSectionBlock(
		slack.NewTextBlockObject("mrkdwn", "*Your [KIND] has been removed for the following reasons:*", false, false),
		nil,
		slack.NewAccessory(slack.NewCheckboxGroupsBlockElement(
			"removal_option",
			boxes...,
		)),
		slack.SectionBlockOptionBlockID("reasonSelectBlock"),
	)

	ret = append(ret, reasons, slack.NewDividerBlock())

	return ret
}
