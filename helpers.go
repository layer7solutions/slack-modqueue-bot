package main

import (
	"bytes"
	"encoding/json"
	"math/rand"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/pkg/errors"
	"github.com/slack-go/slack"
	"github.com/ttgmpsn/mira"
	miramodels "github.com/ttgmpsn/mira/models"
)

var userRegexp = regexp.MustCompile(`(?:/?(?:user|u)/)?([\w-]{3,20})(?:/\w+)?`)
var redditRegexps = [...]*regexp.Regexp{
	// tried from top to bottom
	// http://redd.it/85ucwb
	regexp.MustCompile(`^https?://(?:redd\.it|reddit\.com)/(?P<post>[[:alnum:]]+)`),
	// https://www.reddit.com/tb/85ucwb
	regexp.MustCompile(`^https?://(?:\w+\.)?reddit\.com/tb/(?P<post>[[:alnum:]]+)`),
	// MODMAIL: https://www.reddit.com/message/messages/b8xdaj
	regexp.MustCompile(`^https?://(?:\w+\.)?reddit\.com/message/messages/(?P<modmail>[[:alnum:]]+)`),
	// MODMAIL: https://mod.reddit.com/mail/all/3ba6u
	regexp.MustCompile(`^https?://(?:\w+\.)?reddit\.com/mail/[[:alnum:]]+/(?P<modmailnew>[[:alnum:]]+)`),
	// https://www.reddit.com/r/DestinyTheGame/comments/85ucwb/d2_weekly_reset_thread_20180320/dw03oet/
	regexp.MustCompile(`^https?://(?:\w+\.)?reddit\.com/(?:r/[[:alnum:]]+/)?comments/[[:alnum:]]+/(?:[\w-_.]+)?/(?P<comment>[[:alnum:]]+)`),
	// https://www.reddit.com/r/DestinyTheGame/comments/85ucwb/d2_weekly_reset_thread_20180320/
	regexp.MustCompile(`^https?://(?:\w+\.)?reddit\.com/(?:r/[[:alnum:]]+/)?comments/(?P<post>[[:alnum:]]+)`),
}

/*
This function returns the reddit username for a variety of formats.
Supported are:
	"/u/Oryxhasnonuts",
	"/user/Oryxhasnonuts",
	"/user/Oryxhasnonuts/",
	"user/Oryxhasnonuts",
	"u/Oryxhasnonuts",
	"Oryxhasnonuts",
	"/Oryxhasnonuts",
	"https://mobilesucks.redd.it/user/Oryxhasnonuts/",
	"http://anyfuckingdomain.doicare/user/Oryxhasnonuts/",
	"https://www.reddit.com/user/Oryxhasnonuts/",
	"https://www.reddit.com/user/Oryxhasnonuts/overview",
*/

func parseUser(user string) string {
	check := user
	if u, err := url.Parse(user); err == nil {
		check = u.Path
	}
	check = strings.TrimPrefix(check, "reddit.com")
	if strings.HasPrefix(check, "/r/") || strings.HasPrefix(check, "r/") {
		return ""
	}
	m := userRegexp.FindStringSubmatch(check)

	if len(m) != 2 {
		return ""
	}

	return m[1]
}

func parseURL(url string) (thing string, isModMail bool) {
	for _, r := range redditRegexps {
		m := r.FindStringSubmatch(url)
		if len(m) != 2 {
			continue
		}
		switch r.SubexpNames()[1] {
		case "post":
			thing = "t3_" + m[1]
		case "comment":
			thing = "t1_" + m[1]
		case "modmail":
			isModMail = true
			thing = "t4_" + m[1]
		case "modmailnew":
			isModMail = true
			thing = m[1]
		default:
			panic("redditRegexp: unknown type")
		}
		break
	}

	return
}

func parseURLToUser(url string) string {
	thing, isModMail := parseURL(url)
	if thing == "" {
		return ""
	}

	if isModMail {
		ans, err := reddit.GetModMailByID(thing, false)
		if err != nil {
			return ""
		}
		return ans.User.Name
	}

	// Try to get via reddit api
	log.WithField("prefix", "parseURLToUser").Debugf("trying via reddit api for thing %s", thing)
	var thingR *mira.Reddit
	if strings.HasPrefix(thing, "t3_") {
		thingR = reddit.Post(thing)
	} else if strings.HasPrefix(thing, "t1_") {
		thingR = reddit.Comment(thing)
	} else {
		return ""
	}

	var i miramodels.Submission
	var err error
	if i, err = thingR.SubmissionInfo(); err != nil {
		log.WithField("prefix", "parseURLToUser").WithError(err).Warnf("couldn't find author for submission %s via reddit api", thing)
		return ""

	}
	if i.GetSubreddit() != config.Subreddit {
		return ""
	}
	return i.GetAuthor()
}

func slackSendResponse(url string, msg *slack.Msg) error {
	data, err := json.Marshal(msg)
	if err != nil {
		return errors.Wrap(err, "json marshal")
	}

	var req *http.Request
	req, err = http.NewRequest("POST", url, bytes.NewReader(data))
	if err != nil {
		return errors.Wrap(err, "creating request failed")
	}
	req.Header.Set("Content-Type", "application/json")
	client := http.DefaultClient

	_, err = client.Do(req)
	if err != nil {
		return errors.Wrap(err, "http request failed")
	}

	return nil
}

func insertZWS(s string) string {
	// https://www.fileformat.info/info/unicode/char/200B/index.htm
	zws, _ := utf8.DecodeRune([]byte("\xE2\x80\x8B"))
	var buffer bytes.Buffer
	for i, rune := range s {
		buffer.WriteRune(rune)
		if i%2 == 0 {
			buffer.WriteRune(zws)
		}
	}
	return buffer.String()
}

func trimString(s string, n int) string {
	if len(s) < n {
		return s
	}
	return s[:n] + "…"
}

func randomPersonEmoji() string {
	rand.Seed(time.Now().Unix())
	return ":" + slackEmojiPersons[rand.Intn(len(slackEmojiPersons))] + ":"
}

var slackEmojiPersons = [...]string{
	"person_frowning",
	"person_climbing",
	"person_doing_cartwheel",
	"person_in_lotus_position",
	"person_in_steamy_room",
	"person_with_blond_hair",
	"person_with_headscarf",
	"person_with_pouting_face",
	"bearded_person",
	"bow",
	"information_desk_person",
	"merperson",
	"pray",
	"raised_hands",
	"raising_hand",
	"person_with_ball",
	"person_with_headscarf",
}
