package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/go-pg/pg/v9"
	"github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
	"github.com/ttgmpsn/mira"
	miramodels "github.com/ttgmpsn/mira/models"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"

	"github.com/gofiber/basicauth"
	"github.com/gofiber/compression"
	"github.com/gofiber/fiber"
	"github.com/gofiber/requestid"
)

type botConfig struct {
	ListenHost string
	ListenPort int
	LogFile    string
	Subreddit  string

	Slack struct {
		SigningSecret     string
		VerificationToken string
		BotUserToken      string
		WebPassword       string
		ControlChannel    string
		QueueChannel      string
	}

	Database struct {
		Username string
		Password string
		Host     string
		Database string
	}

	Reddit mira.Credentials
}

var config botConfig
var db *pg.DB

var slackAPI *slack.Client
var slackInfo *slack.AuthTestResponse
var slackUsers usersStruct
var addNoteStates addNoteStatesStruct

var reddit *mira.Reddit

var controlChannel string
var queueChannel string

func init() {
	logrus.SetFormatter(&prefixed.TextFormatter{})
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetOutput(os.Stdout)

	addNoteStates = addNoteStatesStruct{s: make(map[string]addNoteState)}
	slackUsers = usersStruct{u: make(map[string]dbUser)}
}

var log = logrus.WithField("prefix", "main")

func main() {
	var logfile, conffile string
	flag.StringVar(&logfile, "log", "", "path to a logfile (optional, stdout & debug enabled when skipped)")
	flag.StringVar(&conffile, "conf", "", "path to a configfile (required)")
	flag.Parse()

	if logfile != "" {
		f, err := os.OpenFile(logfile, os.O_TRUNC|os.O_WRONLY|os.O_CREATE, 0660)
		if err != nil {
			panic("error opening log file")
		}
		defer f.Close()
		logrus.SetOutput(f)
		logrus.SetLevel(logrus.InfoLevel)
	}

	var err error

	log.Info("Starting DTG SnooMod Slack Bot")
	log.Info("Reading config")
	if len(conffile) == 0 {
		log.Fatal("Please set the conf flag")
	}
	if _, err = toml.DecodeFile(conffile, &config); err != nil {
		log.WithError(err).WithField("file", conffile).Fatal("reading config")
	}

	controlChannel = config.Slack.ControlChannel
	queueChannel = config.Slack.QueueChannel

	// Connect to DB
	db = pg.Connect(&pg.Options{
		User:     config.Database.Username,
		Password: config.Database.Password,
		Database: config.Database.Database,
		Addr:     config.Database.Host,
	})
	//db.AddQueryHook(dbLogger{})

	// listen to Slack
	log.Info("Setting up slack RTM listener")
	slackAPI = slack.New(
		config.Slack.BotUserToken,
		slack.OptionDebug(false),
	)

	slackInfo, err = slackAPI.AuthTest()
	if err != nil {
		log.WithError(err).Fatal("Slack Auth failed")
	}
	log.Infof("Connected to Slack as %s (ID %s)", slackInfo.User, slackInfo.UserID)

	var users []dbUser
	if err = db.Model(&users).Select(); err != nil {
		panic(err)
	}
	for _, user := range users {
		/*if user.Name != "Try_to_guess_my_psn" {
			continue
		}*/
		if err = slackUsers.Add(user); err != nil {
			slackAPI.PostMessage(controlChannel, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
				slack.MsgOptionText(fmt.Sprintf("*WARNING*: Snoonote login failed for User <@%s> (/u/%s): %s", user.SlackUID, user.Name, err), false))
		}
	}

	// Login to Reddit
	reddit = mira.Init(config.Reddit)
	err = reddit.LoginAuth()
	if err != nil {
		panic(err)
	}
	rMeObj, err := reddit.Me().Info()
	if err != nil {
		panic(err)
	}
	rMe, _ := rMeObj.(*miramodels.Me)
	log.WithField("prefix", "reddit").Infof("Connected to Reddit as /u/%s", rMe.Name)

	// Connect to Slack
	rtm := slackAPI.NewRTM()
	go rtm.ManageConnection()
	go processSlackQueue()

	clearChannel()
	buildQueue(config.Subreddit)

	go func() {
		for {
			time.Sleep(time.Minute * 1)
			buildQueue(config.Subreddit)
		}
	}()

	web := fiber.New(&fiber.Settings{
		StrictRouting:         true,
		CaseSensitive:         true,
		DisableStartupMessage: true,
		ETag:                  true,
	})
	web.Use(compression.New())
	web.Use(requestid.New())
	//web.Use(logger.New())

	web.Post("/slash", handleSlash)
	web.Post("/interactive", handleInteractive)
	web.Post("/event", handleEvent)
	web.Get("/noteImg", handleNoteImg)
	web.Get("/redditAuth", handleRedditAuth)
	web.Get("/ping", func(c *fiber.Ctx) {
		c.Send("pong")
	})

	auth := basicauth.New(basicauth.Config{
		Users: map[string]string{
			"mod": config.Slack.WebPassword,
		},
	})
	web.Get("/log/search", auth, handleLogSearch)
	web.Get("/log/:channel?/:date?", auth, handleLog)

	log.Info("Listening on ", fmt.Sprintf("%s:%d", config.ListenHost, config.ListenPort))
	log.Error(web.Listen(fmt.Sprintf("%s:%d", config.ListenHost, config.ListenPort)))
}
