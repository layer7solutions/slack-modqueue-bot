package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/gofiber/fiber"
	"github.com/pkg/errors"
	"github.com/slack-go/slack"
	"github.com/ttgmpsn/mira"
	miramodels "github.com/ttgmpsn/mira/models"
	"golang.org/x/oauth2"
)

var amRemovalCache = make(map[miramodels.RedditID]string)
var redditScopes = []string{"submit", "edit", "flair", "modconfig", "modcontributors", "modflair", "modlog", "modmail", "modposts", "modwiki", "read", "report", "wikiread", "identity", "privatemessages", "wikiedit"}

var authflowModals = make(map[string]string) // slack UID -> modal ID

func redditAuth(slackUID, refreshToken string) (*mira.Reddit, error) {
	r := mira.Init(config.Reddit)
	r.OAuthConfig.Scopes = redditScopes
	r.SetToken(&oauth2.Token{
		AccessToken:  "VOID",
		RefreshToken: refreshToken,
		Expiry:       time.Now().Add(time.Minute * -1),
	}, redditScopes, redditHandleRefreshToken(slackUID))

	if _, err := r.Me().Info(); err != nil {
		return nil, err
	}

	return r, nil
}

// God this is super ugly ._.
func redditHandleRefreshToken(slackUID string) mira.TokenNotifyFunc {
	return func(t *oauth2.Token) error {
		return slackUsers.UpdateRedditToken(slackUID, t.RefreshToken)
	}
}

func getAutomodRemoval(sub string, ID miramodels.RedditID) string {
	if _, ok := amRemovalCache[ID]; !ok {
		if err := fillAutomodRemovals(sub); err != nil {
			log.WithField("prefix", "reddit").WithError(err).Warnf("Couldn't populate amRemovalCache")
		}
	}
	if val, ok := amRemovalCache[ID]; ok {
		return val
	}
	return ""
}

func fillAutomodRemovals(sub string) error {
	sr := reddit.Subreddit(sub)
	ml, e := sr.ModLog(100, "AutoModerator")
	if e != nil {
		return e
	}
	for _, ma := range ml {
		amRemovalCache[ma.TargetFullname] = ma.Details
	}
	return nil
}

func handleRedditAuth(c *fiber.Ctx) {
	l := log.WithField("prefix", "handleRedditAuth")

	slackUID := c.Query("state")
	oauthCode := c.Query("code")
	if len(slackUID) == 0 || len(oauthCode) == 0 {
		l.Warn("no slackUID (state) provided")
		c.Status(fiber.StatusNotFound)
		return
	}
	l = l.WithField("slackUID", slackUID)
	modalID, ok := authflowModals[slackUID]
	if !ok {
		l.Warn("user has no active modal")
		c.Status(fiber.StatusFailedDependency)
		return
	}
	ur := mira.Init(config.Reddit)
	ur.OAuthConfig.Scopes = redditScopes
	var err error
	if err = ur.CodeAuth(oauthCode, redditHandleRefreshToken(slackUID)); err == nil {
		_, err = ur.Me().Info()
	}
	var modal slack.ModalViewRequest
	if err == nil {
		l.Info("Connected to Reddit!")
		slackUsers.SetReddit(slackUID, ur)
		modal = getModalForSignUp(slackUID)
	} else {
		l.WithError(err).Error("reddit sign in failed")
		delete(authflowModals, slackUID)
		modal = slack.ModalViewRequest{
			Type:  slack.VTModal,
			Title: slack.NewTextBlockObject("plain_text", "SnooSnoo Sign-Up", false, false),
			Blocks: slack.Blocks{BlockSet: []slack.Block{
				slack.NewSectionBlock(
					slack.NewTextBlockObject("mrkdwn", fmt.Sprintf("*Step 1* - Reddit :heavy_multiplication_x:\nSorry, there was an issue :white_frowning_face: Maybe the following text helps, if not please contact ttgmpsn:\n_error %s_", err), false, false),
					nil, nil,
				),
				slack.NewDividerBlock(),
			}},
			Close:           slack.NewTextBlockObject("plain_text", "Close", false, false),
			PrivateMetadata: "",
			CallbackID:      "modal_signup",
			ClearOnClose:    true,
			NotifyOnClose:   false,
		}
	}

	a, b := slackAPI.UpdateView(modal, "", "", modalID)
	if b != nil {
		temp, err := json.MarshalIndent(modal, "", "    ")
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(string(temp))
		fmt.Printf("%#v %#v", a, b)
	}
	c.Set("Content-Type", "text/html")
	c.Send("Please check slack. You may close this window.\n<script>window.close();</script>")
}

func getModalForSignUp(slackUID string) slack.ModalViewRequest {
	l := log.WithField("prefix", "getModalForSignUp").WithField("UID", slackUID)
	l.Debug("checking if reddit exists")
	r, ok := slackUsers.GetReddit(slackUID)
	var redditUser string
	if ok {
		l.Debug("reddit exists, checking if login works")
		rMeObj, err := r.Me().Info()
		if err != nil {
			l.WithError(err).Warnf("reddit login failed, does not work anymore")
			slackUsers.DeleteReddit(slackUID)
		}
		rMe, _ := rMeObj.(*miramodels.Me)
		l.Infof("Already connected to Reddit as /u/%s", rMe.Name)
		redditUser = rMe.Name
	}

	// Step 0 - No Reddit
	if len(redditUser) == 0 {
		l.Debugf("no reddit, sending step 0")
		r = mira.Init(config.Reddit)
		url := r.AuthCodeURL(slackUID, redditScopes)
		linkButton := slack.NewButtonBlockElement("signup_action", "open-reddit", slack.NewTextBlockObject("plain_text", ":lock_with_ink_pen: Authorize", true, false))
		linkButton.URL = url
		sec := slack.NewSectionBlock(
			slack.NewTextBlockObject("mrkdwn", "*Step 1* - Reddit :timer_clock:\nPlease click the button and allow the app on Reddit. Return to this window after you've clicked on 'Allow' :sparkles:", false, false),
			nil,
			slack.NewAccessory(linkButton),
		)

		return slack.ModalViewRequest{
			Type:            slack.VTModal,
			Title:           slack.NewTextBlockObject("plain_text", "SnooSnoo Sign-Up", false, false),
			Blocks:          slack.Blocks{BlockSet: []slack.Block{sec}},
			Close:           slack.NewTextBlockObject("plain_text", "Cancel", false, false),
			PrivateMetadata: "",
			CallbackID:      "modal_signup",
			ClearOnClose:    true,
			NotifyOnClose:   true,
		}
	}

	rUnlink := slack.NewButtonBlockElement("signup_action", "unlink-reddit", slack.NewTextBlockObject("plain_text", ":unlock: Remove Link", true, false))
	rUnlink.WithStyle(slack.StyleDanger)
	rUnlink.Confirm = slack.NewConfirmationBlockObject(
		slack.NewTextBlockObject("plain_text", "Are you sure?", false, false),
		slack.NewTextBlockObject("plain_text", "You are about to unlink your Reddit account. Doing this will also unlink your SnooNotes account. Please only use this option if there are any issues the bot didn't detect automatically.", false, false),
		slack.NewTextBlockObject("plain_text", "Yes, I'm sure.", false, false),
		slack.NewTextBlockObject("plain_text", "Cancel", false, false),
	)
	blocks := []slack.Block{
		slack.NewSectionBlock(
			slack.NewTextBlockObject("mrkdwn", fmt.Sprintf("*Step 1* - Reddit :heavy_check_mark:\nWelcome /u/%s :robot:", redditUser), false, false),
			nil,
			slack.NewAccessory(rUnlink),
		),
		slack.NewDividerBlock(),
	}

	l.Debug("checking if snoonotes exists")
	_, ok = slackUsers.GetName(slackUID)
	if ok {
		_, err := getUserNotes(slackUID, "DTG_Bot")
		if err != nil {
			l.WithError(err).Warnf("getting notes failed")
			slackUsers.DeleteNotes(slackUID)
			ok = false
		}
	}

	// Step 1 - Reddit Exists, SnooNotes doesn't
	if !ok {
		l.Debugf("no snoonotes, sending step 1")
		linkButton := slack.NewButtonBlockElement("signup_action", "open-snoonotes", slack.NewTextBlockObject("plain_text", ":lock_with_ink_pen: Authorize", true, false))
		linkButton.URL = "https://snoonotes.com/Signin"
		sec := slack.NewSectionBlock(
			slack.NewTextBlockObject("mrkdwn", "*Step 2* - SnooNotes :timer_clock:\nPlease click the button to log into SnooNotes. If the site asks for additional permissions, you do not need to grant them (but it doesn't really matter). Afterwards, click on <https://snoonotes.com/#!/userkey|User Key>, press the big button to get a new key, check your reddit DMs and paste it below :writing_hand:", false, false),
			nil,
			slack.NewAccessory(linkButton),
		)
		inp := slack.NewInputBlock(
			"snoo-key",
			slack.NewTextBlockObject("plain_text", "SnooNotes User Key:", true, false),
			slack.NewPlainTextInputBlockElement(nil, "snoo-key-value"),
		)
		blocks = append(blocks, sec, inp)
		return slack.ModalViewRequest{
			Type:            slack.VTModal,
			Title:           slack.NewTextBlockObject("plain_text", "SnooSnoo Sign-Up", false, false),
			Blocks:          slack.Blocks{BlockSet: blocks},
			Close:           slack.NewTextBlockObject("plain_text", "Cancel", false, false),
			Submit:          slack.NewTextBlockObject("plain_text", "Next >", false, false),
			PrivateMetadata: "",
			CallbackID:      "modal_signup",
			ClearOnClose:    true,
			NotifyOnClose:   true,
		}
	}

	sUnlink := slack.NewButtonBlockElement("signup_action", "unlink-snoonotes", slack.NewTextBlockObject("plain_text", ":unlock: Remove Link", true, false))
	sUnlink.WithStyle(slack.StyleDanger)
	sUnlink.Confirm = slack.NewConfirmationBlockObject(
		slack.NewTextBlockObject("plain_text", "Are you sure?", false, false),
		slack.NewTextBlockObject("plain_text", "You are about to unlink your SnooNotes account. Your Reddit account will stay linked. Please only use this option if there are any issues the bot didn't detect automatically.", false, false),
		slack.NewTextBlockObject("plain_text", "Yes, I'm sure.", false, false),
		slack.NewTextBlockObject("plain_text", "Cancel", false, false),
	)
	blocks = append(blocks,
		slack.NewSectionBlock(
			slack.NewTextBlockObject("mrkdwn", "*Step 2* - SnooNotes :heavy_check_mark:\nYou can read & write notes :notebook_with_decorative_cover:. Sign-Up complete :tada:", false, false),
			nil,
			slack.NewAccessory(sUnlink),
		))

	return slack.ModalViewRequest{
		Type:            slack.VTModal,
		Title:           slack.NewTextBlockObject("plain_text", "SnooSnoo Sign-Up", false, false),
		Blocks:          slack.Blocks{BlockSet: blocks},
		Close:           slack.NewTextBlockObject("plain_text", "Close", false, false),
		PrivateMetadata: "",
		CallbackID:      "modal_signup",
		NotifyOnClose:   true,
		ClearOnClose:    true,
	}
}

var errRedditNotAuthed = errors.New("user not logged in")

func redditApprove(slackUID string, thingID miramodels.RedditID) error {
	uReddit, ok := slackUsers.GetReddit(slackUID)
	if !ok {
		return errRedditNotAuthed
	}

	switch thingID.Type() {
	case miramodels.KPost:
		uReddit.Post(string(thingID))
	case miramodels.KComment:
		uReddit.Comment(string(thingID))
	default:
		return errors.Errorf("can't approve %s, neither post nor comment", thingID)
	}

	return errors.Wrap(uReddit.Approve(), "could not approve")
}

func redditRemove(slackUID string, thingID miramodels.RedditID) error {
	uReddit, ok := slackUsers.GetReddit(slackUID)
	if !ok {
		return errRedditNotAuthed
	}

	switch thingID.Type() {
	case miramodels.KPost:
		uReddit.Post(string(thingID))
	case miramodels.KComment:
		uReddit.Comment(string(thingID))
	default:
		return errors.Errorf("can't remove %s, neither post nor comment", thingID)
	}

	return errors.Wrap(uReddit.Remove(false), "could not remove")
}
