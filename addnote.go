package main

import (
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"time"

	sn "github.com/ttgmpsn/go-snoonotes"

	"github.com/slack-go/slack"
)

type addNoteState struct {
	SlackCID string // Channel (DM) we're talking to the User
	SlackTS  string // TS of the latest message we're working with
	User     string
	Target   string // Reddit Username we want to SnooNote
	NoteURL  string // Reddit URL we want the SnooNote to attach to
	NoteText string
	NoteType int
}

type addNoteStatesStruct struct {
	sync.RWMutex
	s map[string]addNoteState
}

func (a *addNoteStatesStruct) Set(slackUID string, state addNoteState) {
	a.Lock()
	a.s[slackUID] = state
	a.Unlock()
}
func (a *addNoteStatesStruct) Del(slackUID string) {
	a.Lock()
	delete(a.s, slackUID)
	a.Unlock()
}
func (a *addNoteStatesStruct) Get(slackUID string) (addNoteState, bool) {
	a.RLock()
	defer a.RUnlock()

	s, o := a.s[slackUID]
	return s, o
}

func addNoteStart(slackUID, slackCID, url string) {
	l := log.WithField("prefix", "addNoteStart")

	user, okUser := slackUsers.GetName(slackUID)
	if !okUser {
		l.WithField("UID", slackUID).Warn("unknown user")
		return
	}
	l = l.WithField("user", user)

	url = strings.TrimSpace(strings.Split(strings.Trim(url, "<>"), "|")[0])
	warning := ""
	target := parseURLToUser(url)
	if target == "" {
		target = parseUser(url)
		url = "https://reddit.com/u/" + target
		warning = "*WARNING*: You didn't provide a full link. The usernote won't have a link associated with it.\n"
	}
	l = l.WithField("target", target)

	l.Debugf("new note for URL <%s>", url)

	message, err := getUserNotes(slackUID, target)
	if err != nil {
		l.WithError(err).Error("failed to get snoonotes")
		slackAPI.PostMessage(slackCID, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
			slack.MsgOptionAttachments(slack.Attachment{
				Color:    "danger",
				Fallback: fmt.Sprintf("Could not get SnooNotes: %s", err),
				Title:    "Could not fetch SnooNotes",
				Text:     fmt.Sprintf("Error: %s", err),
				Footer:   "If this error keeps occuring, complain to ttgmpsn",
			}),
		)
		return
	}

	slackAPI.PostMessage(slackCID, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
		slack.MsgOptionText(fmt.Sprintf("Previous SnooNotes for <https://www.reddit.com/user/%s|/u/%s>:", target, target), false),
		slack.MsgOptionAttachments(message.Attachments...))
	var options []slack.AttachmentActionOption
	noteTypes, _ := sn.GetNoteTypes(user, config.Subreddit)
	for _, note := range *noteTypes {
		options = append(options, slack.AttachmentActionOption{
			Text:  note.DisplayName,
			Value: fmt.Sprintf("%d", note.NoteTypeID),
		})
	}

	respChan, respTimestamp, _ := slackAPI.PostMessage(slackCID, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
		slack.MsgOptionText(fmt.Sprintf("Add new SnooNote for <https://www.reddit.com/user/%s|/u/%s>:", target, target), false),
		slack.MsgOptionAttachments(slack.Attachment{
			Fallback:   "Upgrade your client please :(",
			CallbackID: "cat_select",
			Text:       warning + "Select a Category:",
			Actions: []slack.AttachmentAction{
				{
					Name:    "cat_list",
					Text:    "Select...",
					Type:    "select",
					Options: options,
				},
				{
					Name:  "cancel",
					Text:  "Cancel",
					Type:  "button",
					Value: "cancel",
				},
			},
		}),
	)

	state := addNoteState{
		SlackCID: respChan,
		SlackTS:  respTimestamp,
		Target:   target,
		NoteURL:  url,
		User:     user,
	}
	addNoteStates.Set(slackUID, state)
	l.Debugf("added state %#v", state)
}

func addNoteText(slackUID, text string, state addNoteState) {
	l := log.WithField("prefix", "addNoteText").WithField("user", state.User).WithField("target", state.Target)

	// Now that we have text, delete "asking for text" message
	slackAPI.DeleteMessage(state.SlackCID, state.SlackTS)

	text = strings.TrimSpace(text)
	l.WithField("text", text)

	l.Debug("Sending dummy note and ask if okay")
	// :TODO: own function for this, with 24 caching
	types, _ := sn.GetNoteTypeMap(state.User, config.Subreddit)
	noteType, ok := types[state.NoteType]
	if !ok {
		noteType = sn.SimpleNoteType{
			DisplayName: "UNKNOWN (TELL SOMEBODY!)",
			ColorCode:   "000",
		}
	}

	_, respTimestamp, _ := slackAPI.PostMessage(state.SlackCID, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
		slack.MsgOptionText(fmt.Sprintf("Add new SnooNote for <https://www.reddit.com/user/%s|/u/%s> (*PREVIEW*):", state.Target, state.Target), false),
		slack.MsgOptionAttachments(slack.Attachment{
			Fallback:   "Upgrade your client please :(",
			Color:      "%s",
			Title:      text,
			TitleLink:  state.NoteURL,
			Footer:     fmt.Sprintf("*%s* by <https://www.reddit.com/user/%s|%s>", noteType.DisplayName, state.User, state.User),
			FooterIcon: "https://snoonotes.com/favicon.ico",
			Ts:         json.Number(fmt.Sprintf("%d", time.Now().Unix())),
			CallbackID: "note_submit",
			Actions: []slack.AttachmentAction{
				{
					Name:  "okay",
					Text:  "Submit",
					Type:  "button",
					Value: "yes",
					Style: "primary",
				},
				{
					Name:  "okay",
					Text:  "Cancel",
					Type:  "button",
					Value: "no",
					Style: "default", // danger will be red
				},
			},
		}),
	)

	state.NoteText = text
	state.SlackTS = respTimestamp
	addNoteStates.Set(slackUID, state)
	l.Debugf("updated state %#v", state)
}
