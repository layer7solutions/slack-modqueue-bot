package main

import (
	"encoding/json"
	"fmt"
	"html"
	"net/http"
	"net/url"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/gofiber/fiber"
	"github.com/pkg/errors"
	"github.com/slack-go/slack"
	sn "github.com/ttgmpsn/go-snoonotes"
	miramodels "github.com/ttgmpsn/mira/models"
)

type modalMetadata struct {
	ThingID  miramodels.RedditID
	Username string
	URL      string
	Handled  bool // Handled will be true when the post has been dealt with & the ban/note modal is shown
}

func handleInteractive(c *fiber.Ctx) {
	l := log.WithField("prefix", "interactive")

	httpHeaders := http.Header{}
	c.Fasthttp.Request.Header.VisitAll(func(key, value []byte) {
		httpHeaders.Add(string(key), string(value))
	})

	// Verify signing secret
	sv, err := slack.NewSecretsVerifier(httpHeaders, config.Slack.SigningSecret)
	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		l.WithError(err).Errorf("failed to verify SigningSecret")
		return
	}
	sv.Write([]byte(c.Body()))
	if err := sv.Ensure(); err != nil {
		c.Status(fiber.StatusUnauthorized)
		l.WithError(err).Error("failed to verify SigningSecret")
		return
	}

	// Parse request body
	str, _ := url.QueryUnescape(c.Body())
	str = strings.Replace(str, "payload=", "", 1)
	var msg slack.InteractionCallback
	if err := json.Unmarshal([]byte(str), &msg); err != nil {
		c.Status(fiber.StatusInternalServerError)
		log.WithError(err).Error("failed to unmarshal json")
		return
	}
	if len(msg.User.ID) > 0 {
		l = l.WithField("UID", msg.User.ID)
	}

	switch msg.Type {
	case slack.InteractionTypeBlockActions:
		l.Info("Received Block Interaction")
		if len(msg.ActionCallback.BlockActions) != 1 {
			l.Debugf("Num BlockActions is %d", len(msg.ActionCallback.BlockActions))
		}
		if len(msg.ActionCallback.BlockActions) < 1 {
			l.Warn("Num Blockactions is 0")
			c.Status(fiber.StatusBadRequest)
			return
		}
		ba := msg.ActionCallback.BlockActions[0]
		l = l.WithField("action", ba.ActionID)
		switch ba.ActionID {
		case "queueadmin":
			if ba.SelectedOption.Value != "rebuild" {
				l.Warnf("unknown selected option %s", ba.SelectedOption.Value)
				c.Status(fiber.StatusBadRequest)
				return
			}
			clearChannel()
			buildQueue(config.Subreddit)
		case "submission_actions", "thing_actions":
			if ba.SelectedOption.Value != "open-reddit" {
				l.Warnf("unknown selected option %s", ba.SelectedOption.Value)
				c.Status(fiber.StatusBadRequest)
				return
			}
			// No response
			return
		case "action_approve":
			thingID := miramodels.RedditID(ba.Value)
			l := l.WithField("thingID", thingID)
			l.Debug("approving")
			err := redditApprove(msg.User.ID, thingID)
			if err != nil {
				l.WithError(err).Error("could not approve item")
				if err == errRedditNotAuthed {
					view, err := slackAPI.OpenView(msg.TriggerID, getModalForSignUp(msg.User.ID))
					if err != nil {
						l.WithError(err).Error("could not show modal")
						c.Status(fiber.StatusInternalServerError)
						return
					}
					l.Infof("shown signup modal (ID %s) to user", view.ID)
					authflowModals[msg.User.ID] = view.ID
				} else {
					slackSendUserError(msg.User.ID, err)
				}
				return
			}

			slackAPI.DeleteMessage(queueChannel, msg.Message.Timestamp)
			if ts, ok := postedQueue[ba.Value]; ok {
				if ts != msg.Message.Timestamp {
					slackAPI.DeleteMessage(queueChannel, ts)
				}
				delete(postedQueue, ba.Value)
			}
			l.Info("approved")
			return
		case "action_remove":
			thingID := miramodels.RedditID(ba.Value)
			l := l.WithField("thingID", thingID)
			slackAPI.DeleteMessage(queueChannel, msg.Message.Timestamp)
			if ts, ok := postedQueue[ba.Value]; ok {
				if ts != msg.Message.Timestamp {
					slackAPI.DeleteMessage(queueChannel, ts)
				}
				delete(postedQueue, ba.Value)
			}

			l.Debug("creating popup to remove post")
			uReddit, ok := slackUsers.GetReddit(msg.User.ID)
			_, okNotes := slackUsers.GetName(msg.User.ID)
			if !ok || !okNotes {
				l.Warn("user not logged into reddit")
				view, err := slackAPI.OpenView(msg.TriggerID, getModalForSignUp(msg.User.ID))
				if err != nil {
					l.WithError(err).Error("could not show modal")
					c.Status(fiber.StatusInternalServerError)
					return
				}
				l.Infof("shown signup modal (ID %s) to user", view.ID)
				authflowModals[msg.User.ID] = view.ID
				return
			}

			// showing the modal might take longer than 3 secs. so just push a "waiting" modal, and then update it later!
			tempModal := slack.ModalViewRequest{
				Type:  slack.VTModal,
				Title: slack.NewTextBlockObject("plain_text", "Remove Reddit Thing", true, false),
				Blocks: slack.Blocks{BlockSet: []slack.Block{slack.NewSectionBlock(
					slack.NewTextBlockObject("plain_text", ":clock5: Please wait... :clock10:", true, false),
					nil, nil,
				)}},
				Close:         slack.NewTextBlockObject("plain_text", "Cancel + Approve", false, false),
				CallbackID:    "modal_remove",
				ClearOnClose:  true,
				NotifyOnClose: true,
			}
			tempView, err := slackAPI.OpenView(msg.TriggerID, tempModal)
			if err != nil {
				l.WithError(err).Error("could not push temp modal")
				c.Status(fiber.StatusInternalServerError)
				return
			}

			thing, err := uReddit.SubmissionInfoID(thingID)
			if err != nil {
				l.WithError(err).Warn("couldn't find submission")
				return
			}
			// Banned aka removed but not by AM? --> handled
			if len(thing.GetBanned().Mod) > 0 && thing.GetBanned().Mod != "AutoModerator" {
				slackAPI.UpdateView(slack.ModalViewRequest{
					Type:  slack.VTModal,
					Title: slack.NewTextBlockObject("plain_text", "Remove Reddit Thing", true, false),
					Blocks: slack.Blocks{BlockSet: []slack.Block{slack.NewSectionBlock(
						slack.NewTextBlockObject("plain_text", "Thing has already been removed, you can close this window now.", true, false),
						nil, nil,
					)}},
					Close:         slack.NewTextBlockObject("plain_text", "OK", false, false),
					CallbackID:    "modal_remove",
					ClearOnClose:  true,
					NotifyOnClose: false,
				}, "", "", tempView.View.ID)
				l.Info("already removed, silently skipping modal")
				return
			}

			l.Info("removing")
			if err = redditRemove(msg.User.ID, thingID); err != nil {
				l.WithError(err).Error("could not remove item")
				modal := slack.ModalViewRequest{
					Type:  slack.VTModal,
					Title: slack.NewTextBlockObject("plain_text", "Error", true, false),
					Blocks: slack.Blocks{BlockSet: []slack.Block{
						slack.NewSectionBlock(
							slack.NewTextBlockObject("mrkdwn", fmt.Sprintf(":warning: *Error!* The last action could not be completed:\n\n>%s\n\nYou can close this window and try again.", err), false, false),
							nil, nil,
						),
						slack.NewContextBlock("", slack.NewTextBlockObject("plain_text", "If this error keeps ocuring, please report it to ttgmpsn.", false, false)),
					}},
					Close:      slack.NewTextBlockObject("plain_text", "Back", false, false),
					CallbackID: "modal_note",
				}
				_, err = slackAPI.UpdateView(modal, "", "", tempView.View.ID)
				if err != nil {
					l.WithError(err).Error("could not push modal")
					c.Status(fiber.StatusInternalServerError)
					return
				}
				return
			}

			meta := modalMetadata{ThingID: thingID, Username: thing.GetAuthor(), URL: thing.GetURL()}
			metaStr, _ := json.Marshal(meta)

			modal := slack.ModalViewRequest{
				Type:            slack.VTModal,
				Title:           slack.NewTextBlockObject("plain_text", "Remove Reddit Thing", true, false),
				Blocks:          slack.Blocks{BlockSet: submissionToModal(thing, msg.User.ID)},
				Close:           slack.NewTextBlockObject("plain_text", "Cancel + Approve", false, false),
				Submit:          slack.NewTextBlockObject("plain_text", "Remove", false, false),
				PrivateMetadata: string(metaStr),
				CallbackID:      "modal_remove",
				ClearOnClose:    true,
				NotifyOnClose:   true,
			}
			_, err = slackAPI.UpdateView(modal, "", "", tempView.View.ID)
			if err != nil {
				l.WithError(err).Error("could not show modal")
				c.Status(fiber.StatusInternalServerError)
				return
			}
			return
		case "removal_option":
			meta := modalMetadata{}
			if err = json.Unmarshal([]byte(msg.View.PrivateMetadata), &meta); err != nil {
				l.WithError(err).Warnf("received removal_option request with invalid metadata '%s'", msg.View.PrivateMetadata)
				c.Status(fiber.StatusBadRequest)
				return
			}
			thingID := meta.ThingID
			l := l.WithField("thingID", thingID)
			l.Debugf("user selected removal options %#v", ba.SelectedOptions)
			blocks := msg.View.Blocks.BlockSet

			var selectBlock *slack.CheckboxGroupsBlockElement
			reasonsListed := make(map[int]*slack.InputBlock)
			var firstInpBlock int
			for n, block := range blocks {
				if secBlock, ok := block.(*slack.SectionBlock); ok {
					if secBlock.BlockID == "reasonSelectBlock" {
						selectBlock = secBlock.Accessory.CheckboxGroupsBlockElement
					}
				}
				if inpBlock, ok := block.(*slack.InputBlock); ok {
					if firstInpBlock == 0 {
						firstInpBlock = n
					}
					inpN, err := strconv.Atoi(inpBlock.BlockID)
					if err != nil {
						continue
					}
					reasonsListed[inpN] = inpBlock
				}
			}

			var selectedOptions []*slack.OptionBlockObject
			reasonsSelected := make(map[int]*slack.InputBlock)
			for _, o := range ba.SelectedOptions {
				rN, err := strconv.Atoi(o.Value)
				if err != nil {
					l.Warnf("%s is no valid rule number", o.Value)
					continue
				}
				for _, s := range selectBlock.Options {
					if s.Value == o.Value {
						selectedOptions = append(selectedOptions, s)
					}
				}
				rc, err := getRemovalConfig(config.Subreddit)
				if err != nil {
					l.WithError(err).Error("could not get removal reasons")
					c.Status(fiber.StatusInternalServerError)
					return
				}
				r := rc.Reasons[rN]
				if rl, ok := reasonsListed[rN]; ok {
					reasonsSelected[rN] = rl
					continue
				}

				var blck slack.BlockElement
				if len(r.SubReasons) == 1 && strings.HasPrefix(r.SubReasons[0], "<textarea") {
					inp := slack.NewPlainTextInputBlockElement(
						nil, fmt.Sprintf("rule%d_input", rN),
					)
					inp.Multiline = true
					blck = inp
				} else {
					reasons := []*slack.OptionBlockObject{}
					for n, rs := range r.SubReasons {
						reasons = append(reasons, slack.NewOptionBlockObject(
							fmt.Sprintf("%d", n), slack.NewTextBlockObject("plain_text", trimString(rs, 64), true, false), nil,
						))
					}
					sel := slack.NewOptionsSelectBlockElement(
						"static_select",
						slack.NewTextBlockObject("plain_text", "Select...", true, false),
						fmt.Sprintf("rule%d_select", rN),
						reasons...,
					)
					sel.InitialOption = reasons[0]
					blck = sel
				}
				reasonsSelected[rN] = slack.NewInputBlock(
					o.Value,
					slack.NewTextBlockObject("plain_text", r.Title, true, false),
					blck,
				)
			}
			selectBlock.InitialOptions = selectedOptions

			if firstInpBlock != 0 {
				blocks = blocks[:firstInpBlock]
			}
			var keys []int
			for k := range reasonsSelected {
				keys = append(keys, k)
			}
			sort.Ints(keys)
			for _, b := range keys {
				blocks = append(blocks, reasonsSelected[b])
			}

			modal := slack.ModalViewRequest{
				Type:            msg.View.Type,
				Title:           msg.View.Title,
				Blocks:          slack.Blocks{BlockSet: blocks},
				Close:           msg.View.Close,
				Submit:          msg.View.Submit,
				PrivateMetadata: msg.View.PrivateMetadata,
				CallbackID:      msg.View.CallbackID,
				ClearOnClose:    msg.View.ClearOnClose,
				NotifyOnClose:   msg.View.NotifyOnClose,
			}

			_, err = slackAPI.UpdateView(modal, "", "", msg.View.ID)
			if err != nil {
				l.WithError(err).Error("could not update modal")
				c.Status(fiber.StatusInternalServerError)
				return
			}
		case "note_action":
			switch ba.Value {
			default:
				l.Warnf("unknwon note_action option %s", ba.Value)
				c.Status(fiber.StatusNotFound)
				return
			case "open-profile":
				// No Response
				return
			case "open-notes":
				meta := modalMetadata{}
				if err = json.Unmarshal([]byte(msg.View.PrivateMetadata), &meta); err != nil {
					l.WithError(err).Warnf("received note_action request with invalid metadata '%s'", msg.View.PrivateMetadata)
					c.Status(fiber.StatusBadRequest)
					return
				}
				l := l.WithField("username", meta.Username)

				blocks := []slack.Block{}
				profileButton := slack.NewButtonBlockElement("note_action", "open-profile", slack.NewTextBlockObject("plain_text", fmt.Sprintf("%s Show on Reddit", randomPersonEmoji()), true, false))
				profileButton.URL = fmt.Sprintf("https://www.reddit.com/u/%s", url.PathEscape(meta.Username))
				title := slack.NewSectionBlock(
					slack.NewTextBlockObject("mrkdwn",
						fmt.Sprintf(":notebook_with_decorative_cover: SnooNotes for <https://www.reddit.com/u/%s|/u/%s>", url.PathEscape(meta.Username), meta.Username),
						false, false),
					nil,
					slack.NewAccessory(profileButton),
				)
				blocks = append(blocks, title, slack.NewDividerBlock())

				user, okUser := slackUsers.GetName(msg.User.ID)
				if !okUser {
					l.Warn("user not signed into snoonotes?")
					c.Status(fiber.StatusForbidden)
					return
				}
				types, err := sn.GetNoteTypeMap(user, config.Subreddit)
				if err != nil {
					l.WithError(err).Errorf("error getting note type map")
					c.Status(fiber.StatusInternalServerError)
					return
				}
				notes, err := sn.Get(config.Subreddit, user, meta.Username)
				if err != nil {
					l.WithError(err).Errorf("error getting notes")
					c.Status(fiber.StatusInternalServerError)
					return
				}
				if notes != nil {
					for i := range *notes {
						note := (*notes)[len(*notes)-1-i]
						noteType, ok := types[note.NoteTypeID]
						if !ok {
							noteType = sn.SimpleNoteType{
								DisplayName: "UNKNOWN (TELL SOMEBODY!)",
								ColorCode:   "000",
							}
						}
						var t time.Time
						t, err = time.Parse(time.RFC3339Nano, note.TimeStamp)
						if err != nil {
							t = time.Now()
						}
						linkButton := slack.NewButtonBlockElement("note_action", "open-notes", slack.NewTextBlockObject("plain_text", ":link: Link", true, false))
						linkButton.URL = note.URL
						sec := slack.NewSectionBlock(
							slack.NewTextBlockObject("plain_text", note.Message, false, false),
							nil,
							slack.NewAccessory(linkButton),
						)
						context := slack.NewContextBlock(
							"",
							slack.NewImageBlockElement(fmt.Sprintf("%s?c=%s", noteURL, noteType.ColorCode), fmt.Sprintf("Note with color %s", noteType.ColorCode)),
							slack.NewTextBlockObject("plain_text", noteType.DisplayName, false, false),
							slack.NewTextBlockObject("plain_text", fmt.Sprintf(":male-police-officer: /u/%s", note.Submitter), false, false),
							slack.NewTextBlockObject("mrkdwn", fmt.Sprintf(":clock4: <!date^%d^{date_num} {time_secs}|%s>", t.Unix(), t.Format(time.UnixDate)), false, false),
						)
						blocks = append(blocks, sec, context, slack.NewDividerBlock())
					}
				} else {
					blocks = append(blocks, slack.NewSectionBlock(slack.NewTextBlockObject("mrkdwn", "No notes found!", false, false), nil, nil))
				}

				modal := slack.ModalViewRequest{
					Type:       slack.VTModal,
					Title:      slack.NewTextBlockObject("plain_text", "SnooNotes", true, false),
					Blocks:     slack.Blocks{BlockSet: blocks},
					Close:      slack.NewTextBlockObject("plain_text", "Close", false, false),
					CallbackID: "modal_note",
				}
				_, err = slackAPI.PushView(msg.TriggerID, modal)
				if err != nil {
					l.WithError(err).Error("could not push modal")
					c.Status(fiber.StatusInternalServerError)
					return
				}
			}
		case "signup_action":
			switch ba.Value {
			case "open":
				view, err := slackAPI.OpenView(msg.TriggerID, getModalForSignUp(msg.User.ID))
				if err != nil {
					l.WithError(err).Error("could not show modal")
					c.Status(fiber.StatusInternalServerError)
					return
				}
				l.Infof("shown modal (ID %s) to user", view.ID)
				authflowModals[msg.User.ID] = view.ID
			case "open-reddit", "open-snoonotes":
				return
			case "unlink-reddit":
				slackUsers.Delete(msg.User.ID)
				fallthrough
			case "unlink-snoonotes":
				slackUsers.DeleteNotes(msg.User.ID)
				modalID, ok := authflowModals[msg.User.ID]
				if !ok {
					l.Warn("user has no active modal")
					return
				}
				if _, err := slackAPI.UpdateView(getModalForSignUp(msg.User.ID), "", "", modalID); err != nil {
					l.WithError(err).Error("couldn't update modal")
				}
			default:
				l.Warnf("unknown selected option %s", ba.Value)
				c.Status(fiber.StatusBadRequest)
				return
			}
		case "note-cat-select":
			return
		// These all need a single link
		case "dtgbot-twab", "dtgbot-replies", "modremove":
			strings := map[string][]string{
				"dtgbot-twab":    {"Post/Update TWAB", "Please input a reddit link if an existing post should be updated, or provide a link to the TWAB on Bungie.net if the bot should make a new post:"},
				"dtgbot-replies": {"Rescan Bungie Replies", "Please enter the reddit link to the thread that should be rescanned:"},
				"modremove":      {":x: Delete with Reason", "Enter the link to a post/comment that should be removed:"},
			}
			_, err := slackAPI.OpenView(msg.TriggerID, slack.ModalViewRequest{
				Type:  slack.VTModal,
				Title: slack.NewTextBlockObject("plain_text", strings[ba.ActionID][0], true, false),
				Blocks: slack.Blocks{BlockSet: []slack.Block{
					slack.NewInputBlock("url",
						slack.NewTextBlockObject("plain_text", strings[ba.ActionID][1], true, false),
						slack.NewPlainTextInputBlockElement(
							slack.NewTextBlockObject("plain_text", "URL…", false, false),
							"url-value",
						),
					)},
				},
				Close:         slack.NewTextBlockObject("plain_text", "Close", false, false),
				Submit:        slack.NewTextBlockObject("plain_text", "Submit", false, false),
				CallbackID:    fmt.Sprintf("modal_%s", ba.ActionID),
				ClearOnClose:  true,
				NotifyOnClose: false,
			})
			if err != nil {
				l.WithError(err).Error("could not show modal")
				c.Status(fiber.StatusInternalServerError)
				return
			}
		case "dtgbot-reset":
			urlInput := slack.NewInputBlock("url",
				slack.NewTextBlockObject("plain_text", "If the bot should update an existing thread, input the URL below (leave blank if it should make a new one):", true, false),
				slack.NewPlainTextInputBlockElement(
					slack.NewTextBlockObject("plain_text", "URL…", false, false),
					"url-value",
				))
			urlInput.Optional = true
			_, err := slackAPI.OpenView(msg.TriggerID, slack.ModalViewRequest{
				Type:  slack.VTModal,
				Title: slack.NewTextBlockObject("plain_text", "Post/Update Reset Thread", true, false),
				Blocks: slack.Blocks{BlockSet: []slack.Block{
					slack.NewInputBlock("type",
						slack.NewTextBlockObject("plain_text", "Select the Thread Type", true, false),
						slack.NewOptionsSelectBlockElement(
							"static_select",
							slack.NewTextBlockObject("plain_text", "Select...", true, false),
							"type-value",
							slack.NewOptionBlockObject("daily", slack.NewTextBlockObject("plain_text", "Daily Reset Thread", false, false), nil),
							slack.NewOptionBlockObject("weekly", slack.NewTextBlockObject("plain_text", "Weekly Reset Thread", false, false), nil),
							slack.NewOptionBlockObject("trials", slack.NewTextBlockObject("plain_text", "Trials of Osiris Megathread", false, false), nil),
							slack.NewOptionBlockObject("xur", slack.NewTextBlockObject("plain_text", "Xûr Megathread", false, false), nil),
							slack.NewOptionBlockObject("ib", slack.NewTextBlockObject("plain_text", "Iron Banner Megathread", false, false), nil),
						),
					),
					urlInput,
				}},
				Close:         slack.NewTextBlockObject("plain_text", "Close", false, false),
				Submit:        slack.NewTextBlockObject("plain_text", "Submit", false, false),
				CallbackID:    fmt.Sprintf("modal_%s", ba.ActionID),
				ClearOnClose:  true,
				NotifyOnClose: false,
			})
			if err != nil {
				l.WithError(err).Error("could not show modal")
				c.Status(fiber.StatusInternalServerError)
				return
			}
		case "dtgbot-ff":
			shortitle := slack.NewInputBlock("shorttitle",
				slack.NewTextBlockObject("plain_text", "(Optional) Enter a short title for the Sidebar Link", true, false),
				slack.NewPlainTextInputBlockElement(
					slack.NewTextBlockObject("plain_text", "Shorttitle…", false, false),
					"shorttitle-value",
				),
			)
			shortitle.Optional = true
			_, err := slackAPI.OpenView(msg.TriggerID, slack.ModalViewRequest{
				Type:  slack.VTModal,
				Title: slack.NewTextBlockObject("plain_text", "Post Focused Feedback", true, false),
				Blocks: slack.Blocks{BlockSet: []slack.Block{
					slack.NewInputBlock("title",
						slack.NewTextBlockObject("plain_text", "Enter the full title of the Focused Feedback", true, false),
						slack.NewPlainTextInputBlockElement(
							slack.NewTextBlockObject("plain_text", "Full Title…", false, false),
							"title-value",
						),
					),
					shortitle,
				}},
				Close:         slack.NewTextBlockObject("plain_text", "Close", false, false),
				Submit:        slack.NewTextBlockObject("plain_text", "Submit", false, false),
				CallbackID:    fmt.Sprintf("modal_%s", ba.ActionID),
				ClearOnClose:  true,
				NotifyOnClose: false,
			})
			if err != nil {
				l.WithError(err).Error("could not show modal")
				c.Status(fiber.StatusInternalServerError)
				return
			}
		case "dtgbot-banner":
			style, err := reddit.Subreddit(config.Subreddit).Stylesheet()
			if err != nil {
				l.WithError(err).Error("could not get subreddit stylesheet")
				c.Status(fiber.StatusInternalServerError)
				return
			}
			images := []*slack.OptionBlockObject{}
			flairRegex := regexp.MustCompile(`^SS[0-9]{1,}$`)
			for _, img := range style.Images {
				if flairRegex.MatchString(img.Name) {
					continue
				}
				images = append(images, slack.NewOptionBlockObject(img.Name, slack.NewTextBlockObject("plain_text", img.Name, false, false), nil))
			}
			_, err = slackAPI.OpenView(msg.TriggerID, slack.ModalViewRequest{
				Type:  slack.VTModal,
				Title: slack.NewTextBlockObject("plain_text", "Update Banner Image", true, false),
				Blocks: slack.Blocks{BlockSet: []slack.Block{
					slack.NewSectionBlock(
						slack.NewTextBlockObject("mrkdwn", fmt.Sprintf("Click <https://www.reddit.com/r/%s/about/stylesheet|here> to see all available images or to upload new ones!", config.Subreddit), false, false),
						nil, nil,
					),
					slack.NewInputBlock("image",
						slack.NewTextBlockObject("plain_text", "Select image ", true, false),
						slack.NewOptionsSelectBlockElement(
							"static_select",
							slack.NewTextBlockObject("plain_text", "Select...", true, false),
							"image-value",
							images...,
						),
					),
				}},
				Close:         slack.NewTextBlockObject("plain_text", "Close", false, false),
				Submit:        slack.NewTextBlockObject("plain_text", "Submit", false, false),
				CallbackID:    fmt.Sprintf("modal_%s", ba.ActionID),
				ClearOnClose:  true,
				NotifyOnClose: false,
			})
			if err != nil {
				l.WithError(err).Error("could not show modal")
				c.Status(fiber.StatusInternalServerError)
				return
			}
		default:
			l.Warnf("unknown block action %s", ba.ActionID)
			c.Status(fiber.StatusNotImplemented)
			return
		}
	case slack.InteractionTypeViewClosed:
		l = l.WithField("CallbackID", msg.View.CallbackID)
		l.Info("Received View Closed Interaction")
		switch msg.View.CallbackID {
		default:
			l.Warn("unknwon callback ID")
			c.Status(fiber.StatusNotFound)
			return
		case "modal_signup":
			delete(authflowModals, msg.User.ID)
			updateAppHome(msg.User.ID)
			return
		case "modal_remove":
			meta := modalMetadata{}
			if err = json.Unmarshal([]byte(msg.View.PrivateMetadata), &meta); err != nil {
				l.WithError(err).Warnf("received removal_option request with invalid metadata '%s'", msg.View.PrivateMetadata)
				c.Status(fiber.StatusBadRequest)
				return
			}
			if meta.Handled {
				return
			}
			thingID := meta.ThingID
			l := l.WithField("thingID", thingID)
			l.Debug("approving")
			err := redditApprove(msg.User.ID, thingID)
			if err != nil {
				l.WithError(err).Error("could not approve item")
				slackSendUserError(msg.User.ID, err)
				return
			}
			l.Info("approved")
			return
		}
	case slack.InteractionTypeViewSubmission:
		l = l.WithField("CallbackID", msg.View.CallbackID)
		l.Info("Received View Submission Interaction")
		switch msg.View.CallbackID {
		default:
			l.Warn("unknwon callback ID")
			c.Status(fiber.StatusNotFound)
			return
		case "modal_remove":
			meta := modalMetadata{}
			if err = json.Unmarshal([]byte(msg.View.PrivateMetadata), &meta); err != nil {
				l.WithError(err).Warnf("received removal_option request with invalid metadata '%s'", msg.View.PrivateMetadata)
				c.Status(fiber.StatusBadRequest)
				return
			}
			thingID := meta.ThingID
			l := l.WithField("thingID", thingID)
			if !meta.Handled {
				// Remove post, show Ban/Note
				rc, err := getRemovalConfig(config.Subreddit)
				if err != nil {
					l.WithError(err).Error("could not get removal reasons")
					c.Status(fiber.StatusInternalServerError)
					return
				}
				l.Debug("building removal reason text")
				var remText string
				keys := make([]string, 0, len(msg.View.State.Values))
				for k := range msg.View.State.Values {
					keys = append(keys, k)
				}
				sort.Strings(keys)
				for _, rN := range keys {
					r := msg.View.State.Values[rN]
					for _, rr := range r {
						if rr.Type == "static_select" {
							rNi, err := strconv.Atoi(rN)
							if err != nil {
								l.WithError(err).Warnf("couldn't convert rule number '%s' to int", rN)
							}
							srNi, err := strconv.Atoi(rr.SelectedOption.Value)
							if err != nil {
								l.WithError(err).Warnf("couldn't convert subrule number '%s' to int", rr.SelectedOption.Value)
							}
							remText = fmt.Sprintf("%s\n\n%s", remText, rc.Reasons[rNi].SubReasons[srNi])
						} else if rr.Type == "plain_text_input" {
							remText = fmt.Sprintf("%s\n\n%s", remText, rr.Value)
						}
					}
				}
				kind := "*unknown*"
				if thingID.Type() == miramodels.KPost {
					kind = "submission"
				} else if thingID.Type() == miramodels.KComment {
					kind = "comment"
				} else {
					l.Warn("unknown thingID type")
				}
				if len(remText) > 0 {
					kind := "*unknown*"
					if thingID.Type() == miramodels.KPost {
						kind = "submission"
					} else if thingID.Type() == miramodels.KComment {
						kind = "comment"
					} else {
						l.Warn("unknown thingID type")
					}
					remText = toolboxReplace(fmt.Sprintf("%s%s\n\n%s", rc.Header, remText, rc.Footer), map[string]string{"kind": kind})
					uReddit, _ := slackUsers.GetReddit(msg.User.ID)
					if comment, err := uReddit.ReplyWithID(string(thingID), remText); err != nil || len(comment.JSON.Data.Things) < 1 {
						l.WithError(err).Warn("reply couldn't be posted")
					} else {
						l.Info("posted reply on reddit")
						cID := comment.JSON.Data.Things[0].Data.GetID()
						uReddit.Comment(string(cID)).Distinguish("yes", false)
					}
				} else {
					l.Info("no removal reason selected, skipping posting")
				}

				// Build after-removal popup
				blocks := []slack.Block{
					slack.NewSectionBlock(
						slack.NewTextBlockObject("mrkdwn", fmt.Sprintf("The <%s|%s> has been successfully removed. If you want to add a note or ban the user, you can fill in the boxes below. Note that the ban reason & duration will be noted automatically.", meta.URL, kind), false, false),
						nil, nil,
					),
					slack.NewDividerBlock(),
				}
				user, _ := slackUsers.GetName(msg.User.ID)
				types, err := sn.GetNoteTypeMap(user, config.Subreddit)
				if err != nil {
					l.WithError(err).Errorf("error getting note type map")
					return
				}
				notes, err := sn.Get(config.Subreddit, user, meta.Username)
				if err != nil {
					l.WithError(err).Errorf("error getting notes")
					c.Status(fiber.StatusInternalServerError)
					return
				}
				if notes != nil {
					note := (*notes)[len(*notes)-1]
					noteType, ok := types[note.NoteTypeID]
					if !ok {
						noteType = sn.SimpleNoteType{
							DisplayName: "UNKNOWN (TELL SOMEBODY!)",
							ColorCode:   "000",
						}
					}
					var t time.Time
					t, err = time.Parse(time.RFC3339Nano, note.TimeStamp)
					if err != nil {
						t = time.Now()
					}
					notes := slack.NewSectionBlock(
						slack.NewTextBlockObject("mrkdwn", fmt.Sprintf("*Last Note*: %s", note.Message), false, false),
						nil,
						slack.NewAccessory(slack.NewButtonBlockElement("note_action", "open-notes", slack.NewTextBlockObject("plain_text", fmt.Sprintf(":notebook_with_decorative_cover: Open all %d Notes", len(*notes)), true, false))),
					)
					context := slack.NewContextBlock(
						"",
						slack.NewImageBlockElement(fmt.Sprintf("%s?c=%s", noteURL, noteType.ColorCode), fmt.Sprintf("Note with color %s", noteType.ColorCode)),
						slack.NewTextBlockObject("plain_text", noteType.DisplayName, false, false),
						slack.NewTextBlockObject("plain_text", fmt.Sprintf(":male-police-officer: /u/%s", note.Submitter), false, false),
						slack.NewTextBlockObject("mrkdwn", fmt.Sprintf(":clock4: <!date^%d^{date_num} {time_secs}|%s>", t.Unix(), t.Format(time.UnixDate)), false, false),
					)
					blocks = append(blocks, notes, context)
				} else {
					blocks = append(blocks, slack.NewSectionBlock(
						slack.NewTextBlockObject("mrkdwn", "*Last Note*: _no previous notes_", false, false),
						nil, nil))
				}

				// Note Block
				var noteOptions []*slack.OptionBlockObject
				for noteTypeID, noteType := range types {
					noteOptions = append(noteOptions, slack.NewOptionBlockObject(strconv.Itoa(noteTypeID), slack.NewTextBlockObject("plain_text", noteType.DisplayName, false, false), nil))
				}
				noteCatBlock := slack.NewInputBlock(
					"note-cat",
					slack.NewTextBlockObject("plain_text", "New Note Category:", false, false),
					slack.NewOptionsSelectBlockElement(
						"static_select",
						slack.NewTextBlockObject("plain_text", "Select...", true, false),
						"note-cat",
						noteOptions...,
					),
				)
				noteCatBlock.Optional = true
				noteTextBlock := slack.NewInputBlock(
					"note-text",
					slack.NewTextBlockObject("plain_text", "New Note Text:", true, false),
					slack.NewPlainTextInputBlockElement(
						nil, "note-text",
					),
				)
				noteTextBlock.Optional = true

				// Ban block
				banText := slack.NewPlainTextInputBlockElement(
					nil, "ban-reason",
				)
				banText.Multiline = true
				banTextBlock := slack.NewInputBlock(
					"ban-reason",
					slack.NewTextBlockObject("plain_text", "Ban Reason (DM'd to the User):", true, false),
					banText,
				)
				banTextBlock.Optional = true
				banReasonBlock := slack.NewInputBlock(
					"ban-note",
					slack.NewTextBlockObject("plain_text", "Ban Note (for the mod log & note)", true, false),
					slack.NewPlainTextInputBlockElement(
						nil, "ban-note",
					),
				)
				banReasonBlock.Optional = true
				banDurationBlock := slack.NewInputBlock(
					"ban-duration",
					slack.NewTextBlockObject("plain_text", "Ban Duration (number of days, 'perma' or 'shadow')", true, false),
					slack.NewPlainTextInputBlockElement(
						nil, "ban-duration",
					),
				)
				banDurationBlock.Optional = true

				// Putting it together & static blocks
				blocks = append(blocks,
					noteCatBlock,
					noteTextBlock,
					slack.NewDividerBlock(),
					banDurationBlock,
					banReasonBlock,
					banTextBlock,
				)

				meta.Handled = true
				metaStr, _ := json.Marshal(meta)

				modal := slack.ModalViewRequest{
					Type:            slack.VTModal,
					Title:           slack.NewTextBlockObject("plain_text", "Remove Reddit Thing", true, false),
					Blocks:          slack.Blocks{BlockSet: blocks},
					Close:           slack.NewTextBlockObject("plain_text", "Close", false, false),
					Submit:          slack.NewTextBlockObject("plain_text", "Ban/Note + Close", false, false),
					PrivateMetadata: string(metaStr),
					CallbackID:      "modal_remove",
					ClearOnClose:    true,
					NotifyOnClose:   true,
				}
				resp := slack.NewUpdateViewSubmissionResponse(&modal)
				c.JSON(resp)
			} else {
				// Handle Note/Ban Submission
				uReddit, ok := slackUsers.GetReddit(msg.User.ID)
				snooUser, okNotes := slackUsers.GetName(msg.User.ID)
				if !ok || !okNotes {
					l.Error("could not get reddit or notes instance")
					c.Status(fiber.StatusInternalServerError)
					return
				}

				values := make(map[string]string)
				for key, val1 := range msg.View.State.Values {
					val2, ok := val1[key]
					if !ok {
						l.Warnf("%s didn't have sub-key", key)
						continue
					}
					if val2.Type == "static_select" {
						values[key] = val2.SelectedOption.Value
					} else {
						values[key] = val2.Value
					}
				}
				// Ban Note or Reason given, but no duration --> show error
				if (len(values["ban-note"]) > 0 || len(values["ban-reason"]) > 0) && len(values["ban-duration"]) == 0 {
					c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"ban-duration": "You provided a note/reason but no duration."}))
					return
				}
				var noteText string
				var noteCat int
				if len(values["ban-duration"]) != 0 {
					banDays, err := strconv.Atoi(values["ban-duration"])
					if err != nil && values["ban-duration"] != "perma" && values["ban-duration"] != "shadow" {
						c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"ban-duration": "Invalid duration. Please enter the number of days, 'perma' or 'shadow'."}))
						return
					}
					if values["ban-duration"] == "shadow" {
						var automod *miramodels.Wiki
						automod, err = uReddit.Subreddit(config.Subreddit).Wiki("config/automoderator")
						if err != nil || len(automod.ContentMD) == 0 {
							c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"ban-duration": "Shadow did not work. Please enter the number of days or 'perma'."}))
							return
						}
						automodOld := html.UnescapeString(automod.ContentMD)
						automodR := regexp.MustCompile(`(?i)(### Shadowbans\s+author:\s+name \(full-exact\): \[.*)(\]\s+action: remove\s+action_reason: User is AutoMod Shadowbanned)`)
						automodNew := automodR.ReplaceAllString(automodOld, fmt.Sprintf(`$1, %s$2`, meta.Username))
						if automodNew == automodOld {
							c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"ban-duration": "Shadow did not work. Please enter the number of days or 'perma'."}))
							return
						}
						uReddit.Subreddit(config.Subreddit).EditWiki("config/automoderator", automodNew, fmt.Sprintf("shadow %s", meta.Username))
					} else {
						uReddit.Subreddit(config.Subreddit).Ban(meta.Username, banDays, string(meta.ThingID), values["ban-reason"], values["ban-note"])
					}
					noteText = fmt.Sprintf("Note: %s - ", values["ban-note"])
					if values["ban-duration"] == "shadow" {
						noteText += "shadowban"
						noteCat = 18
					} else if values["ban-duration"] == "perma" {
						noteText += "permanent"
						noteCat = 17
					} else {
						noteText += fmt.Sprintf("%d days", banDays)
						noteCat = 16
					}
					noteText += fmt.Sprintf("\n\nMessage: %s", values["ban-reason"])
				}
				if len(values["note-text"]) != 0 {
					selCat, _ := strconv.Atoi(values["note-cat"])
					if noteCat == 0 || selCat != 0 {
						noteCat = selCat
					}
					if noteCat == 0 {
						c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"note-cat": "Please make a selection to add a note."}))
						return
					}
					if len(noteText) != 0 {
						noteText = fmt.Sprintf("%s\n\n%s", values["note-text"], noteText)
					} else {
						noteText = values["note-text"]
					}
				}
				if len(noteText) != 0 {
					note := sn.NewNote{
						NoteTypeID:        noteCat,
						SubName:           config.Subreddit,
						Message:           noteText,
						AppliesToUsername: meta.Username,
						URL:               meta.URL,
					}
					l.Debugf("submitting note %#v", note)
					if err = sn.Add(snooUser, note); err != nil {
						l.WithError(err).Error("unable to add note")
						c.Status(fiber.StatusInternalServerError)
						return
					}
				}
			}
		case "modal_signup":
			var key string
			if a, ok := msg.View.State.Values["snoo-key"]; ok {
				if b, ok := a["snoo-key-value"]; ok {
					key = b.Value
				}
			}
			if len(key) == 0 {
				l.Warn("got empty key")
				c.Status(fiber.StatusBadRequest)
				return
			}
			var resp *slack.ViewSubmissionResponse
			if err := slackUsers.SetNotes(msg.User.ID, key); err != nil {
				l.WithError(err).Error("couldn't sign into snoonotes")
				resp = slack.NewErrorsViewSubmissionResponse(map[string]string{"snoo-key": "Please check the value. Login to SnooNotes failed."})

			} else {
				modal := getModalForSignUp(msg.User.ID)
				resp = slack.NewUpdateViewSubmissionResponse(&modal)
			}
			c.JSON(resp)
			return
		case "modal_modremove":
			var url string
			if a, ok := msg.View.State.Values["url"]; ok {
				if b, ok := a["url-value"]; ok {
					url = b.Value
				}
			}
			thing, modmail := parseURL(url)
			if len(url) == 0 || len(thing) == 0 || modmail {
				c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"url": "Please provide a valid URL"}))
				return
			}
			modal, err := showModRemove(msg.User.ID, url)
			if err != nil {
				c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"url": err.Error()}))
				return
			}
			resp := slack.NewUpdateViewSubmissionResponse(&modal)
			c.JSON(resp)
			return
		case "modal_dtgbot-twab":
			var url string
			if a, ok := msg.View.State.Values["url"]; ok {
				if b, ok := a["url-value"]; ok {
					url = b.Value
				}
			}
			r := regexp.MustCompile(`^https?://(?:\w+\.)?bungie\.net/(?:.*)News(?:/Article)?/(?P<newsid>[0-9]+)`)
			m := r.FindStringSubmatch(url)
			thing, modmail := parseURL(url)
			if (len(url) == 0 || len(thing) == 0 || !strings.HasPrefix(thing, "t3_") || modmail) && len(m) != 2 {
				c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"url": "Please provide a valid URL to a reddit post or Bungie Blog article."}))
				return
			}
			var additionaltext string
			if len(m) == 2 {
				l.Debugf("Bungie.net link provided (%s), making new post as bot", url)
				res, err := http.Get(url)
				if err != nil {
					c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"url": "URL could not be retrieved. Is it valid?"}))
					return
				}
				defer res.Body.Close()
				doc, err := goquery.NewDocumentFromReader(res.Body)
				if err != nil {
					c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"url": "URL could not be retrieved. Is it valid?"}))
					return
				}
				title := strings.TrimSpace(strings.Split(doc.Find("title").Text(), ">")[0])
				post, err := reddit.Subreddit("DestinyTheGame").Submit(title, fmt.Sprintf("Source: https://www.bungie.net/en/News/Article/%s\n\n---\n\nPlease stand by while this post is updated…", m[1]))
				if err != nil {
					c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"url": "Post could not be created. Is the link valid?"}))
					return
				}
				url = post.JSON.Data.URL
				l.Debugf("Created reddit post as %s", url)
				additionaltext = fmt.Sprintf("A new post has been created <%s|here>. Please sticky as appropriate! ", url)
			}

			var modal slack.ModalViewRequest
			uReddit, ok := slackUsers.GetReddit(msg.User.ID)
			if !ok {
				modal = getModalForSignUp(msg.User.ID)
			} else {
				uReddit.Redditor("DTG_Bot").Compose("BungieBlog", url)
				modal = slack.ModalViewRequest{
					Type:  slack.VTModal,
					Title: slack.NewTextBlockObject("plain_text", "Post/Update TWAB", true, false),
					Blocks: slack.Blocks{BlockSet: []slack.Block{
						slack.NewSectionBlock(slack.NewTextBlockObject("plain_text", additionaltext+":robot_face: DTG_Bot has been instructed to update the TWAB :arrows_counterclockwise:. You should receive a Reddit DM once it is done :incoming_envelope:!", true, false), nil, nil),
					}},
					Close:         slack.NewTextBlockObject("plain_text", "Close", false, false),
					ClearOnClose:  true,
					NotifyOnClose: false,
				}
			}
			resp := slack.NewPushViewSubmissionResponse(&modal)
			c.JSON(resp)
			return
		case "modal_dtgbot-replies":
			var url string
			if a, ok := msg.View.State.Values["url"]; ok {
				if b, ok := a["url-value"]; ok {
					url = b.Value
				}
			}
			thing, modmail := parseURL(url)
			if len(url) == 0 || len(thing) == 0 || !strings.HasPrefix(thing, "t3_") || modmail {
				c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"url": "Please provide a valid URL to a reddit post."}))
				return
			}
			var modal slack.ModalViewRequest
			uReddit, ok := slackUsers.GetReddit(msg.User.ID)
			if !ok {
				modal = getModalForSignUp(msg.User.ID)
			} else {
				uReddit.Redditor("DTG_Bot").Compose("BungieReplied", url)
				modal = slack.ModalViewRequest{
					Type:  slack.VTModal,
					Title: slack.NewTextBlockObject("plain_text", "Rescan Bungie Replies", true, false),
					Blocks: slack.Blocks{BlockSet: []slack.Block{
						slack.NewSectionBlock(slack.NewTextBlockObject("plain_text", ":robot_face: DTG_Bot has been instructed to update the Bungie Replies :male-teacher:. You should receive a Reddit DM once it is done :incoming_envelope:!", true, false), nil, nil),
					}},
					Close:         slack.NewTextBlockObject("plain_text", "Close", false, false),
					ClearOnClose:  true,
					NotifyOnClose: false,
				}
			}
			resp := slack.NewPushViewSubmissionResponse(&modal)
			c.JSON(resp)
			return
		case "modal_dtgbot-reset":
			var threadType string
			var url string
			if a, ok := msg.View.State.Values["url"]; ok {
				if b, ok := a["url-value"]; ok {
					url = b.Value
				}
			}
			if a, ok := msg.View.State.Values["type"]; ok {
				if b, ok := a["type-value"]; ok {
					threadType = b.SelectedOption.Value
				}
			}
			thing, modmail := parseURL(url)
			if len(threadType) == 0 {
				c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"type": "Please select a valid type."}))
				return
			}
			if len(url) != 0 && (len(thing) == 0 || !strings.HasPrefix(thing, "t3_") || modmail) {
				c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"url": "Please provide a valid URL to a reddit post or leave this field blank."}))
				return
			}
			var modal slack.ModalViewRequest
			uReddit, ok := slackUsers.GetReddit(msg.User.ID)
			if !ok {
				modal = getModalForSignUp(msg.User.ID)
			} else {
				uReddit.Redditor("DTG_Bot").Compose("ResetThread", fmt.Sprintf("%s %s", threadType, url))
				modal = slack.ModalViewRequest{
					Type:  slack.VTModal,
					Title: slack.NewTextBlockObject("plain_text", "Post/Update Reset Thread", true, false),
					Blocks: slack.Blocks{BlockSet: []slack.Block{
						slack.NewSectionBlock(slack.NewTextBlockObject("plain_text", ":robot_face: DTG_Bot has been instructed to update/make the Reset Thread :sunrise:. You should receive a Reddit DM once it is done :incoming_envelope:!", true, false), nil, nil),
					}},
					Close:         slack.NewTextBlockObject("plain_text", "Close", false, false),
					ClearOnClose:  true,
					NotifyOnClose: false,
				}
			}
			resp := slack.NewPushViewSubmissionResponse(&modal)
			c.JSON(resp)
			return
		case "modal_dtgbot-ff":
			var title, shorttitle string
			if a, ok := msg.View.State.Values["title"]; ok {
				if b, ok := a["title-value"]; ok {
					title = b.Value
				}
			}
			if a, ok := msg.View.State.Values["shorttitle"]; ok {
				if b, ok := a["shorttitle-value"]; ok {
					title = b.Value
				}
			}
			if len(title) == 0 {
				c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"title": "Please enter a valid title."}))
				return
			}
			if len(title) < len(shorttitle) {
				c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"shorttitle": "Your short title is longer than your title."}))
				return
			}
			FFmsg := fmt.Sprintf("title: %s", title)
			if len(shorttitle) > 0 {
				FFmsg = fmt.Sprintf("%s\n\nshorttitle: %s", FFmsg, shorttitle)
			}
			var modal slack.ModalViewRequest
			uReddit, ok := slackUsers.GetReddit(msg.User.ID)
			if !ok {
				modal = getModalForSignUp(msg.User.ID)
			} else {
				uReddit.Redditor("DTG_Bot").Compose("FF", FFmsg)
				modal = slack.ModalViewRequest{
					Type:  slack.VTModal,
					Title: slack.NewTextBlockObject("plain_text", "Post Focused Feedback", true, false),
					Blocks: slack.Blocks{BlockSet: []slack.Block{
						slack.NewSectionBlock(slack.NewTextBlockObject("plain_text", ":robot_face: DTG_Bot has been instructed to make the Focused Feedback thread :spiral_note_pad:. You should receive a Reddit DM once it is done :incoming_envelope:!", true, false), nil, nil),
					}},
					Close:         slack.NewTextBlockObject("plain_text", "Close", false, false),
					ClearOnClose:  true,
					NotifyOnClose: false,
				}
			}
			resp := slack.NewPushViewSubmissionResponse(&modal)
			c.JSON(resp)
			return
		case "modal_dtgbot-banner":
			var bannerImage string
			if a, ok := msg.View.State.Values["image"]; ok {
				if b, ok := a["image-value"]; ok {
					bannerImage = b.SelectedOption.Value
				}
			}
			if len(bannerImage) == 0 {
				c.JSON(slack.NewErrorsViewSubmissionResponse(map[string]string{"image": "Please select an image."}))
				return
			}
			var modal slack.ModalViewRequest
			uReddit, ok := slackUsers.GetReddit(msg.User.ID)
			if !ok {
				modal = getModalForSignUp(msg.User.ID)
			} else {
				uReddit.Redditor("DTG_Bot").Compose("Banner", bannerImage)
				modal = slack.ModalViewRequest{
					Type:  slack.VTModal,
					Title: slack.NewTextBlockObject("plain_text", "Update Banner Image", true, false),
					Blocks: slack.Blocks{BlockSet: []slack.Block{
						slack.NewSectionBlock(slack.NewTextBlockObject("plain_text", ":robot_face: DTG_Bot has been instructed to update the banner :frame_with_picture:. You should receive a Reddit DM once it is done :incoming_envelope:!", true, false), nil, nil),
					}},
					Close:         slack.NewTextBlockObject("plain_text", "Close", false, false),
					ClearOnClose:  true,
					NotifyOnClose: false,
				}
			}
			resp := slack.NewPushViewSubmissionResponse(&modal)
			c.JSON(resp)
			return
		}
	case slack.InteractionTypeInteractionMessage:
		l.Info("Received Message Interaction")
		l = l.WithField("CallbackID", msg.CallbackID)
		switch msg.CallbackID {
		default:
			l.Warn("unknwon callback ID")
			c.Status(fiber.StatusNotFound)
			return
		case "cat_select":
			c.Status(fiber.StatusOK)
			message, err := func() (*slack.Msg, error) {
				state, okState := addNoteStates.Get(msg.User.ID)
				if !okState || state.NoteType != 0 {
					return nil, errors.New("invalid state")
				}
				l = l.WithField("user", state.User)

				var actionValue string
				for _, aa := range msg.ActionCallback.AttachmentActions {
					// cancel thing
					if aa.Name == "cancel" && aa.Value == "cancel" {
						addNoteStates.Del(msg.User.ID)
						return &slack.Msg{DeleteOriginal: true}, nil
					}
					if aa.Name != "cat_list" {
						continue
					}
					if len(aa.SelectedOptions) == 1 {
						actionValue = aa.SelectedOptions[0].Value
					}
				}
				if actionValue == "" {
					return nil, errors.New("no actionValue provided")
				}
				actionValueInt, err := strconv.Atoi(actionValue)
				if err != nil {
					return nil, errors.Wrap(err, "couldn't convert actionValue to int")
				}

				types, _ := sn.GetNoteTypeMap(state.User, config.Subreddit)
				noteType, ok := types[actionValueInt]
				if !ok {
					noteType = sn.SimpleNoteType{
						DisplayName: "UNKNOWN (TELL SOMEBODY!)",
						ColorCode:   "000",
					}
				}

				state.NoteType = actionValueInt
				addNoteStates.Set(msg.User.ID, state)

				return &slack.Msg{
					Text:            msg.OriginalMessage.Text,
					ReplaceOriginal: true,
					Attachments: []slack.Attachment{{
						Color: "%s",
						Text:  fmt.Sprintf("Selected Category: %s\nEnter Note Text:", noteType.DisplayName),
					}},
				}, nil
			}()

			if err != nil {
				l.WithError(err).Warn("cat_select failed")
				message = &slack.Msg{
					Attachments: []slack.Attachment{{
						Color:    "danger",
						Fallback: fmt.Sprintf("Error selecting category: %s", err),
						Title:    "Error selecting category!",
						Text:     fmt.Sprintf("Error: %s", err),
						Footer:   "If this error keeps occuring, complain to ttgmpsn",
					}},
				}
			}
			if err = slackSendResponse(msg.ResponseURL, message); err != nil {
				l.WithError(err).Error("sending data to ResponseURL")
				return
			}
			l.Debug("Sent Category response")
		case "note_submit":
			c.Status(fiber.StatusOK)
			message, err := func() (*slack.Msg, error) {
				state, okState := addNoteStates.Get(msg.User.ID)
				if !okState || state.NoteType == 0 || state.NoteText == "" {
					return nil, errors.New("invalid state")
				}
				// delete state (Yeah here, doesn't hurt)
				addNoteStates.Del(msg.User.ID)

				var actionValue string
				for _, aa := range msg.ActionCallback.AttachmentActions {
					if aa.Name != "okay" {
						continue
					}
					actionValue = aa.Value
				}
				if actionValue == "" {
					return nil, errors.New("no actionValue provided")
				}
				if actionValue == "no" {
					return &slack.Msg{DeleteOriginal: true}, nil
				}

				types, _ := sn.GetNoteTypeMap(state.User, config.Subreddit)
				noteType, ok := types[state.NoteType]
				if !ok {
					noteType = sn.SimpleNoteType{
						DisplayName: "UNKNOWN (TELL SOMEBODY!)",
						ColorCode:   "000",
					}
				}

				note := sn.NewNote{
					NoteTypeID:        state.NoteType,
					SubName:           config.Subreddit,
					Message:           state.NoteText,
					AppliesToUsername: state.Target,
					URL:               state.NoteURL,
				}
				l.Debugf("submitting note %#v", note)
				if err := sn.Add(state.User, note); err != nil {
					return nil, err
				}

				return &slack.Msg{
					Text:            fmt.Sprintf("Added new SnooNote for <https://www.reddit.com/user/%s|/u/%s>:", state.Target, state.Target),
					ReplaceOriginal: true,
					Attachments: []slack.Attachment{{
						Color: "%s",

						Title:     state.NoteText,
						TitleLink: state.NoteURL,

						Footer:     fmt.Sprintf("*%s* by <https://www.reddit.com/user/%s|%s>", noteType.DisplayName, state.User, state.User),
						FooterIcon: "https://snoonotes.com/favicon.ico",

						Ts: json.Number(fmt.Sprintf("%d", time.Now().Unix())),
					}},
				}, nil
			}()

			if err != nil {
				l.WithError(err).Warn("note_submit failed")
				message = &slack.Msg{
					Attachments: []slack.Attachment{{
						Color:    "danger",
						Fallback: fmt.Sprintf("Error submitting note: %s", err),
						Title:    "Error submitting note!",
						Text:     fmt.Sprintf("Error: %s", err),
						Footer:   "If this error keeps occuring, complain to ttgmpsn",
					}},
				}
			}
			if err = slackSendResponse(msg.ResponseURL, message); err != nil {
				l.WithError(err).Error("sending data to ResponseURL")
				return
			}
			l.Debug("Sent NoteSubmit response")
		case "snsnote":
			c.Status(fiber.StatusOK)
			message := func() *slack.Msg {
				var user string
				for _, aa := range msg.ActionCallback.AttachmentActions {
					// "Hide"
					if aa.Name != "action" {
						continue
					}
					if aa.Value == "d" {
						return &slack.Msg{DeleteOriginal: true}
					}
					user = aa.Value
				}
				message, err := getUserNotes(msg.User.ID, user)
				if err != nil {
					/*if message == nil {
						l.WithError(err).Error("failed to get snoonotes")
						message = &slack.Msg{
							Attachments: []slack.Attachment{{
								Color:    "danger",
								Fallback: fmt.Sprintf("Could not get SnooNotes: %s", err),
								Title:    "Could not fetch SnooNotes",
								Text:     fmt.Sprintf("Error: %s", err),
								Footer:   "If this error keeps occuring, complain to ttgmpsn",
							}},
						}
					}*/
					return nil
				}
				return message
			}()
			if message == nil {
				return
			}
			go slackAPI.PostMessage(msg.Channel.ID, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
				slack.MsgOptionText(message.Text, false), slack.MsgOptionAttachments(message.Attachments...))
			if err := slackSendResponse(msg.ResponseURL, &slack.Msg{DeleteOriginal: true}); err != nil {
				l.WithError(err).Error("sending data to ResponseURL")
				return
			}
		}
	default:
		l.Warnf("unknown event %s", msg.Type)
		c.Status(fiber.StatusNotFound)
		return
	}
}
