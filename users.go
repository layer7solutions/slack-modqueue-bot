package main

import (
	"fmt"
	"sync"

	"github.com/go-pg/pg/v9"
	"github.com/pkg/errors"
	sn "github.com/ttgmpsn/go-snoonotes"
	"github.com/ttgmpsn/mira"
	miramodels "github.com/ttgmpsn/mira/models"
)

//lint:ignore U1000 tableName required for pg
type dbUser struct {
	tableName          struct{}     `pg:"snoonotes_slackbot"`
	SlackUID           string       `pg:"slack_id,pk"`
	Name               string       `pg:"reddit_username"` // reddit username
	SNAPIKey           string       `pg:"sn_api_key"`
	RedditRefreshToken string       `pg:"reddit_refreshtoken"`
	Reddit             *mira.Reddit `pg:"-"`
	HasNotes           bool         `pg:"-"`
	HasReddit          bool         `pg:"-"`
}

type usersStruct struct {
	sync.RWMutex
	u map[string]dbUser
}

var futureTokens = make(map[string]string)

func (u *usersStruct) get(slackUID string) (dbUser, bool) {
	u.RLock()
	defer u.RUnlock()
	s, ok := u.u[slackUID]
	return s, ok
}

func (u *usersStruct) Add(user dbUser) error {
	if len(user.SNAPIKey) > 0 {
		if err := sn.Auth(user.Name, user.SNAPIKey); err != nil {
			return err
		}
		user.HasNotes = true
	}
	if user.Reddit == nil && len(user.RedditRefreshToken) > 0 {
		var err error
		if user.Reddit, err = redditAuth(user.SlackUID, user.RedditRefreshToken); err != nil {
			return err
		}
	}
	if user.Reddit != nil {
		user.HasReddit = true
	}

	u.Lock()
	u.u[user.SlackUID] = user
	u.Unlock()

	if err := db.Insert(&user); err != nil {
		pgErr, ok := err.(pg.Error)
		if !ok || !pgErr.IntegrityViolation() {
			return errors.Wrap(err, "could not add user to db")
		}
	}

	return nil
}
func (u *usersStruct) GetName(slackUID string) (string, bool) {
	s, ok := u.get(slackUID)

	return s.Name, s.HasNotes && ok
}
func (u *usersStruct) GetReddit(slackUID string) (*mira.Reddit, bool) {
	s, ok := u.get(slackUID)

	return s.Reddit, s.HasReddit && ok
}
func (u *usersStruct) SetNotes(slackUID, apikey string) error {
	s, ok := u.get(slackUID)
	if !ok {
		return fmt.Errorf("user '%s' does not exist", slackUID)
	}
	if err := sn.Auth(s.Name, apikey); err != nil {
		return err
	}

	s.SNAPIKey = apikey
	s.HasNotes = true
	u.Lock()
	u.u[slackUID] = s
	u.Unlock()

	return s.Update()
}
func (u *usersStruct) SetReddit(slackUID string, reddit *mira.Reddit) error {
	s, ok := u.get(slackUID)
	if !ok {
		err := u.Add(dbUser{SlackUID: slackUID})
		if err != nil {
			return err
		}
		s, ok = u.get(slackUID)
		if !ok {
			return fmt.Errorf("user '%s' does not exist and couldn't be added", slackUID)
		}
	}
	rMeObj, err := reddit.Me().Info()
	if err != nil {
		return errors.Wrap(err, "reddit login failed or does not work anymore")
	}
	rMe, _ := rMeObj.(*miramodels.Me)

	s.Reddit = reddit
	s.HasReddit = true
	if len(s.Name) == 0 {
		s.Name = rMe.Name
	}
	if t, ok := futureTokens[slackUID]; ok {
		s.RedditRefreshToken = t
		delete(futureTokens, slackUID)
	}
	u.Lock()
	u.u[slackUID] = s
	u.Unlock()

	return s.Update()
}
func (u *usersStruct) UpdateRedditToken(slackUID, refreshToken string) error {
	s, ok := u.get(slackUID)
	if !ok {
		// Not yet in DB, save for when user is added :)
		futureTokens[slackUID] = refreshToken
		return nil
	}

	s.RedditRefreshToken = refreshToken
	u.Lock()
	u.u[slackUID] = s
	u.Unlock()

	return s.Update()
}
func (u *usersStruct) DeleteReddit(slackUID string) error {
	s, ok := u.get(slackUID)
	if !ok {
		return fmt.Errorf("user '%s' does not exist", slackUID)
	}

	s.Reddit = nil
	s.HasReddit = false
	s.RedditRefreshToken = ""
	u.Lock()
	u.u[slackUID] = s
	u.Unlock()

	return s.Update()
}
func (u *usersStruct) DeleteNotes(slackUID string) error {
	s, ok := u.get(slackUID)
	if !ok {
		return fmt.Errorf("user '%s' does not exist", slackUID)
	}

	s.SNAPIKey = ""
	s.HasNotes = false
	u.Lock()
	u.u[slackUID] = s
	u.Unlock()

	return s.Update()
}
func (u *usersStruct) Delete(slackUID string) {
	s, ok := u.get(slackUID)
	if !ok {
		return
	}

	u.Lock()
	db.Delete(s)
	delete(u.u, slackUID)
	u.Unlock()
}

// Update own database entry
func (s *dbUser) Update() error {
	return db.Update(s)
}
