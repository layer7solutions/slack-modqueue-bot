package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gofiber/fiber"
	"github.com/slack-go/slack"
	miramodels "github.com/ttgmpsn/mira/models"
)

// REPLAE WITH NEW EXAMPLE DUE TO SIGNINGSECRET!
func handleSlash(c *fiber.Ctx) {
	l := log.WithField("prefix", "slash")

	httpHeaders := http.Header{}
	c.Fasthttp.Request.Header.VisitAll(func(key, value []byte) {
		httpHeaders.Add(string(key), string(value))
	})

	verifier, err := slack.NewSecretsVerifier(httpHeaders, config.Slack.SigningSecret)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		l.WithError(err).Errorf("failed to verify SigningSecret")
		return
	}

	s := slack.SlashCommand{}
	s.Token = c.FormValue("token")
	s.TeamID = c.FormValue("team_id")
	s.TeamDomain = c.FormValue("team_domain")
	s.EnterpriseID = c.FormValue("enterprise_id")
	s.EnterpriseName = c.FormValue("enterprise_name")
	s.ChannelID = c.FormValue("channel_id")
	s.ChannelName = c.FormValue("channel_name")
	s.UserID = c.FormValue("user_id")
	s.UserName = c.FormValue("user_name")
	s.Command = c.FormValue("command")
	s.Text = c.FormValue("text")
	s.ResponseURL = c.FormValue("response_url")
	s.TriggerID = c.FormValue("trigger_id")

	verifier.Write([]byte(c.Body()))
	if err = verifier.Ensure(); err != nil {
		l.WithError(err).Errorf("failed to verify SigningSecret")
		c.Status(fiber.StatusUnauthorized)
		return
	}

	l = l.WithField("command", s.Command).WithField("UID", s.UserID)

	switch s.Command {
	case "/modsignup", "/dev_modsignup":
		view, err := slackAPI.OpenView(s.TriggerID, getModalForSignUp(s.UserID))
		if err != nil {
			l.WithError(err).Error("could not show modal")
			c.Status(fiber.StatusInternalServerError)
			return
		}
		l.Infof("shown modal (ID %s) to user", view.ID)
		authflowModals[s.UserID] = view.ID
	case "/modremove", "/dev_modremove":
		modal, err := showModRemove(s.UserID, s.Text)
		if err != nil {
			resp := &slack.Msg{Text: err.Error()}
			resp.ResponseType = slack.ResponseTypeEphemeral
			c.JSON(resp)
			return
		}
		view, err := slackAPI.OpenView(s.TriggerID, modal)
		if err != nil {
			l.WithError(err).Error("could not show modal")
			return
		}
		if modal.CallbackID == "modal_signup" {
			l.Infof("shown signup modal (ID %s) to user", view.ID)
			authflowModals[s.UserID] = view.ID
		}
		return
	case "/sn":
		c.Status(fiber.StatusOK)
		// Call in go func to not provoke any "took too long to respond"
		// messages. We're responding via the provided URL anyways.
		// NOTE: do NOT use w or r *AT ALL* inside the go routine.
		go func() {
			text := strings.TrimSpace(strings.Split(s.Text, " ")[0])
			user := parseURLToUser(text)
			if user == "" {
				user = parseUser(text)
			}
			var message *slack.Msg
			message, err = getUserNotes(s.UserID, user)
			if err != nil {
				if message == nil {
					l.WithError(err).Error("failed to get snoonotes")
					message = &slack.Msg{
						Attachments: []slack.Attachment{{
							Color:    "danger",
							Fallback: fmt.Sprintf("Could not get SnooNotes: %s", err),
							Title:    "Could not fetch SnooNotes",
							Text:     fmt.Sprintf("Error: %s", err),
							Footer:   "If this error keeps occuring, complain to ttgmpsn",
						}},
					}
				}
			} else {
				message.Attachments = append(message.Attachments, slack.Attachment{
					Text:       "Actions:",
					CallbackID: "snsnote",
					Ts:         json.Number(fmt.Sprintf("%d", time.Now().Unix())),
					Actions: []slack.AttachmentAction{
						{
							Name:  "action",
							Text:  "Post to Channel",
							Type:  "button",
							Value: user,
							Style: "primary",
						},
						{
							Name:  "action",
							Text:  "Hide",
							Type:  "button",
							Value: "d",       // min reddit username length is 3, there will never be a conflict.
							Style: "default", // danger will be red
						},
					},
				})
			}
			if err = slackSendResponse(s.ResponseURL, message); err != nil {
				l.WithError(err).Error("sending data to ResponseURL")
				return
			}
		}()
		return
	case "/snhelp":
		c.Status(fiber.StatusOK)
		// Print info directly, no gofunc necessary -> quick
		if err := slackSendResponse(s.ResponseURL, &slack.Msg{
			Text: "Help for DeathBySnooSnoo:",
			Attachments: []slack.Attachment{
				{
					Color:    "warning",
					Fallback: "*/sn* _username|link_: View notes for that User",
					Title:    "Read SnooNotes: */sn* _username/link_",
					Text:     "The output will first be sent to you privately. If you want to share it with the current channel, press the \"Post to Channel\" button. If you want to hide the result instead, press \"Hide\".",
				},
				{
					Color:    "warning",
					Fallback: "*/msg @DeathBySnooSnoo* _username/link_: Add a note for that User",
					Title:    "Adding notes: */msg @DeathBySnooSnoo* _username/link_",
					Text:     "Add a note by pasting the username or link (preferred) into a DM with the bot. You'll then be interactively asked for a category and note text.",
					Footer:   "HINT: This can also be used to quickly check notes on mobile. Simply share a link into the DM channel, and cancel the category prompt!",
				},
				{
					Color:    "warning",
					Fallback: "/modsignup: Register with the Bot",
					Title:    "Register with the Bot: /modsignup",
					Text:     "Just type the command, and follow the cool interactive guide :)",
				},
				{
					Color:    "warning",
					Fallback: "/modremove link: Remove & mod a reddit post/comment",
					Title:    "Remove & mod a reddit post/comment: */modremove* _link_",
					Text:     "With this command, you can bring up the interactive removal & note/ban dialogue from #modqueue on any post or comment.",
					Footer:   "Make sure to check out #modqueue!",
				},
			},
		}); err != nil {
			l.WithError(err).Error("sending data to ResponseURL")
			return
		}
	default:
		l.Warnf("unknown slash command")
		c.Status(fiber.StatusNotFound)
		return
	}
}

// error is only returned if it should be shown to the user
func showModRemove(userID, URL string) (slack.ModalViewRequest, error) {
	l := log.WithField("prefix", "showModRemove").WithField("UID", userID)

	uReddit, ok := slackUsers.GetReddit(userID)
	_, okNotes := slackUsers.GetName(userID)
	if !ok || !okNotes {
		l.Warn("user not logged into reddit")
		return getModalForSignUp(userID), nil
	}

	thingStr, isModMail := parseURL(strings.TrimSpace(strings.Split(URL, " ")[0]))
	var thing miramodels.Submission
	var thingID miramodels.RedditID
	if thingStr != "" && !isModMail {
		thingID = miramodels.RedditID(thingStr)
		l = l.WithField("thingID", thingID)
		if ts, ok := postedQueue[string(thingID)]; ok {
			slackAPI.DeleteMessage(queueChannel, ts)
			delete(postedQueue, string(thingID))
		}

		l.Debug("creating popup to remove post")
		var err error
		thing, err = uReddit.SubmissionInfoID(thingID)
		if thing != nil && err == nil {
			l.Info("removing")
			if err = redditRemove(userID, thingID); err != nil {
				l.WithError(err).Error("could not remove item")
				modal := slack.ModalViewRequest{
					Type:  slack.VTModal,
					Title: slack.NewTextBlockObject("plain_text", "Error", true, false),
					Blocks: slack.Blocks{BlockSet: []slack.Block{
						slack.NewSectionBlock(
							slack.NewTextBlockObject("mrkdwn", fmt.Sprintf(":warning: *Error!* The last action could not be completed:\n\n>%s\n\nYou can close this window and try again.", err), false, false),
							nil, nil,
						),
						slack.NewContextBlock("", slack.NewTextBlockObject("plain_text", "If this error keeps ocuring, please report it to ttgmpsn.", false, false)),
					}},
					Close:      slack.NewTextBlockObject("plain_text", "Back", false, false),
					CallbackID: "modal_note",
				}
				return modal, nil
			}
		}
	}
	if thing == nil || thing.GetSubreddit() != config.Subreddit {
		l.Info("no valid submission found")
		return slack.ModalViewRequest{}, fmt.Errorf("could not find a valid submission under the provided URL :(")
	}

	meta := modalMetadata{ThingID: thingID, Username: thing.GetAuthor(), URL: thing.GetURL()}
	metaStr, _ := json.Marshal(meta)

	modal := slack.ModalViewRequest{
		Type:            slack.VTModal,
		Title:           slack.NewTextBlockObject("plain_text", "Remove Reddit Thing", true, false),
		Blocks:          slack.Blocks{BlockSet: submissionToModal(thing, userID)},
		Close:           slack.NewTextBlockObject("plain_text", "Cancel + Approve", false, false),
		Submit:          slack.NewTextBlockObject("plain_text", "Remove + Note", false, false),
		PrivateMetadata: string(metaStr),
		CallbackID:      "modal_remove",
		ClearOnClose:    true,
		NotifyOnClose:   true,
	}
	return modal, nil
}
