package main

import (
	"fmt"
	"time"

	"github.com/slack-go/slack"
)

type slackMessage struct {
	ChannelID string
	Options   []slack.MsgOption
	ThingID   string
}

var slackQueue chan slackMessage

func init() {
	slackQueue = make(chan slackMessage)
}

// :TODO: Maybe move ThingID somewhere else?
func postMessage(channelID, thingID string, options ...slack.MsgOption) {
	slackQueue <- slackMessage{
		ChannelID: channelID,
		Options:   options,
		ThingID:   thingID,
	}
}

// called as goroutine
func processSlackQueue() {
	for msg := range slackQueue {
		for {
			_, ts, err := slackAPI.PostMessage(msg.ChannelID, msg.Options...)
			if err == nil {
				if len(msg.ThingID) > 0 {
					postedQueue[msg.ThingID] = ts
				}
				break
			}
			if rlErr, ok := err.(*slack.RateLimitedError); ok && rlErr.Retryable() {
				log.WithField("prefix", "slack").Infof("Hit Rate Limit Error, will try again in %d", rlErr.RetryAfter)
				time.Sleep(rlErr.RetryAfter)
				continue // Try again next loop
			}
			log.WithField("prefix", "slack").WithError(err).Warnf("Unable to send message to slack: %#v", msg.Options)
			break
		}
	}
}

func slackSendUserError(slackUID string, err error) {
	slackAPI.PostMessage(slackUID, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
		slack.MsgOptionAttachments(slack.Attachment{
			Color:    "danger",
			Fallback: fmt.Sprintf("Error occured: %s", err),
			Title:    "An Error occured",
			Text:     fmt.Sprintf("An Error occured: %s", err),
			Footer:   "If this error keeps occuring, complain to ttgmpsn",
		}),
	)
}
