package main

import (
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"fmt"
	"html/template"
	"io"
	"math/rand"
	"regexp"
	"strings"
	"time"

	"github.com/go-pg/pg/v9"
	"github.com/gofiber/fiber"
)

type logTplMain struct {
	HeaderExtras string
	ByLine       string
	Base         string

	Index   logTplIndex
	Channel logTplChannel
	Day     logTplDay
	Search  logTplSearch
	Queue   logTplQueue
}

type logTplIndex struct {
	LastMsgs []logSlackMessage
}

type logTplChannel struct {
	Channel logSlackChannel
	Dates   map[uint]map[uint][]time.Time
}

type logTplDay struct {
	Channel logSlackChannel
	Date    time.Time
	Msgs    []logSlackMessage
}

type logTplSearch struct {
	Term    string
	Total   int
	Results []logTplSearchRes
}

type logTplSearchRes struct {
	Msg     logSlackMessage
	Context []logSlackMessage
}

type logTplQueue struct {
	Size []logTplQueueSize
}

//lint:ignore U1000 tableName required for pg
type logTplQueueSize struct {
	tableName struct{} `pg:"dtg_queuesize"`
	Time      time.Time
	Size      uint
}

func handleLog(c *fiber.Ctx) {
	tplVars := logTplMain{
		Base: "/log",
	}
	var contentTpl string
	if len(c.Params("channel")) == 0 {
		contentTpl = "index"

		tplVars.Index = logTplIndex{
			LastMsgs: []logSlackMessage{},
		}

		if err := db.Model(&tplVars.Index.LastMsgs).DistinctOn("channel_id").Relation("User").Relation("Channel").Order("channel_id ASC").Order("time DESC").Select(); err != nil {
			c.Status(fiber.StatusInternalServerError).Send(err)
			return
		}
	} else if len(c.Params("date")) == 0 && c.Params("channel") == "AutoLinker.js" {
		c.SendFile("./logweb/autolinker.js")
		return
	} else if len(c.Params("date")) == 0 && c.Params("channel") == "queue" {
		contentTpl = "queue"

		queue := []logTplQueueSize{}
		if err := db.Model(&queue).Order("time ASC").Select(); err != nil {
			c.Status(fiber.StatusInternalServerError).Send(err)
			return
		}
		filteredQueue := []logTplQueueSize{}
		oldQ := logTplQueueSize{Size: 999}
		for _, q := range queue {
			if q.Size != oldQ.Size {
				filteredQueue = append(filteredQueue, q)
				oldQ = q
			}
		}
		tplVars.Queue.Size = filteredQueue
	} else if len(c.Params("date")) == 0 {
		contentTpl = "channel"

		tplVars.Channel = logTplChannel{
			Channel: logSlackChannel{CID: c.Params("channel")},
			Dates:   make(map[uint]map[uint][]time.Time),
		}

		if err := db.Select(&tplVars.Channel.Channel); err != nil {
			if err == pg.ErrNoRows {
				c.Status(fiber.StatusNotFound)
				return
			}
			c.Status(fiber.StatusInternalServerError).Send(err)
			return
		}

		//lint:ignore U1000 tableName required for pg
		type strAllDates struct {
			tableName struct{} `pg:"slacklog_messages"`
			Day       uint
			Month     uint
			Year      uint
		}
		allDates := []strAllDates{}
		if err := db.Model(&allDates).ColumnExpr("extract(day from time) as day").ColumnExpr("extract(month from time) as month").ColumnExpr("extract(year from time) as year").Where("channel_id = ?", tplVars.Channel.Channel.CID).Group("day").Group("month").Group("year").Order("year DESC").Order("month DESC").Order("day DESC").Select(); err != nil {
			c.Status(fiber.StatusInternalServerError).Send(err)
			return
		}
		for _, date := range allDates {
			if _, ok := tplVars.Channel.Dates[date.Year]; !ok {
				tplVars.Channel.Dates[date.Year] = make(map[uint][]time.Time)
			}
			if _, ok := tplVars.Channel.Dates[date.Year][date.Month]; !ok {
				tplVars.Channel.Dates[date.Year][date.Month] = []time.Time{}
			}
			t, _ := time.Parse("2006-01-02", fmt.Sprintf("%04d-%02d-%02d", date.Year, date.Month, date.Day))
			tplVars.Channel.Dates[date.Year][date.Month] = append(tplVars.Channel.Dates[date.Year][date.Month], t)
		}
	} else {
		contentTpl = "day"

		t, err := time.Parse("2006-01-02", c.Params("date"))
		if err != nil {
			c.Status(fiber.StatusNotFound)
			return
		}

		tplVars.Day = logTplDay{
			Channel: logSlackChannel{CID: c.Params("channel")},
			Date:    t,
		}

		if err := db.Select(&tplVars.Day.Channel); err != nil {
			if err == pg.ErrNoRows {
				c.Status(fiber.StatusNotFound)
				return
			}
			c.Status(fiber.StatusInternalServerError).Send(err)
			return
		}

		if err := db.Model(&tplVars.Day.Msgs).Relation("User").Where("channel_id = ?", tplVars.Day.Channel.CID).Where("time between date ? and ?", tplVars.Day.Date.Format("2006-01-02"), tplVars.Day.Date.AddDate(0, 0, 1).Format("2006-01-02")).Order("time ASC").Select(); err != nil {
			if err == pg.ErrNoRows {
				c.Status(fiber.StatusNotFound)
				return
			}
			c.Status(fiber.StatusInternalServerError).Send(err)
			return
		}
	}

	templates, err := template.ParseFiles(
		"logweb/base.html",
		fmt.Sprintf("logweb/%s.html", contentTpl),
	)
	if err != nil {
		c.Status(fiber.StatusInternalServerError).Send(err)
		return
	}
	var buf bytes.Buffer
	if err := templates.Execute(&buf, tplVars); err != nil {
		c.Status(fiber.StatusInternalServerError).Send(err)
		return
	}
	c.Set(fiber.HeaderContentType, fiber.MIMETextHTML)
	c.SendBytes(buf.Bytes())
}

func handleLogSearch(c *fiber.Ctx) {
	if len(c.Query("term")) == 0 {
		c.Status(fiber.StatusBadRequest)
		return
	}
	tplVars := logTplMain{
		Base: "/log",
		Search: logTplSearch{
			Term: c.Query("term"),
		},
	}

	res := []logSlackMessage{}
	var err error
	if tplVars.Search.Total, err = db.Model(&res).Relation("User").Relation("Channel").Where("message LIKE ?", fmt.Sprintf("%%%s%%", c.Query("term"))).Limit(20).Order("time DESC").SelectAndCount(); err != nil {
		if err != pg.ErrNoRows {
			c.Status(fiber.StatusInternalServerError).Send(err)
			return
		}
	}
	for _, r := range res {
		app := logTplSearchRes{Msg: r}

		pre := []logSlackMessage{}
		if err = db.Model(&pre).Relation("User").Where("channel_id = ?", r.Channel.CID).Where("time < ?", r.Time).Order("time DESC").Limit(2).Select(); err != nil {
			if err != pg.ErrNoRows {
				c.Status(fiber.StatusInternalServerError).Send(err)
				return
			}
		}
		post := []logSlackMessage{}
		if err = db.Model(&post).Relation("User").Where("channel_id = ?", r.Channel.CID).Where("time > ?", r.Time).Order("time ASC").Limit(2).Select(); err != nil {
			if err != pg.ErrNoRows {
				c.Status(fiber.StatusInternalServerError).Send(err)
				return
			}
		}

		app.Context = append(app.Context, pre...)
		app.Context = append(app.Context, r)
		app.Context = append(app.Context, post...)
		tplVars.Search.Results = append(tplVars.Search.Results, app)
	}

	templates, err := template.ParseFiles(
		"logweb/base.html",
		"logweb/search.html",
	)
	if err != nil {
		c.Status(fiber.StatusInternalServerError).Send(err)
		return
	}
	var buf bytes.Buffer
	if err := templates.Execute(&buf, tplVars); err != nil {
		c.Status(fiber.StatusInternalServerError).Send(err)
		return
	}
	c.Set(fiber.HeaderContentType, fiber.MIMETextHTML)
	c.SendBytes(buf.Bytes())
}

func (logTplDay) LogColor(username string) string {
	colors := [...]string{
		"06F", "900", "093", "F0C", "C30", "0C9", "666", "C90", "C36",
		"F60", "639", "630", "966", "69C", "039", "7e1e9c", "15b01a", "0343df",
		"ff81c0", "653700", "e50000", "029386", "f97306", "c20078", "75bbfd",
	}

	h := md5.New()
	io.WriteString(h, username)
	var seed uint64 = binary.BigEndian.Uint64(h.Sum(nil))

	r := rand.New(rand.NewSource(int64(seed)))
	return colors[r.Intn(len(colors))]
}

func (l logTplDay) LogLinker(line string) template.HTML {
	r1 := regexp.MustCompile(`<@([A-Z0-9]{9,11})>`)
	r2 := regexp.MustCompile(`<([^/][^<>@|"]+)>`)
	r3 := regexp.MustCompile(`<#([A-Z0-9]{9})\|([^<>|"]+)>`)
	r4 := regexp.MustCompile(`<([^<>|"]+)\|([^<>|"]+)>`)

	line = r1.ReplaceAllStringFunc(line, func(uStr string) string {
		u := strings.Trim(uStr, "<>@")
		user, ok := logUsers[u]
		if !ok {
			user = logSlackUser{UID: u}
			if err := db.Select(&user); err != nil {
				return uStr
			}
			logUsers[u] = user
		}
		return fmt.Sprintf(`<span style="color: #%s">@%s</span>`, l.LogColor(user.Name), user.Name)
	})
	line = r2.ReplaceAllString(line, `<a href="$1" target="_blank">$1</a>`)
	line = r3.ReplaceAllString(line, `<a href="/log/$1">#$2</a>`)
	line = r4.ReplaceAllString(line, `<a href="$1" target="_blank">$2</a>`)
	return template.HTML(line)
}
