module DTG-SnooMod

go 1.14

require (
	github.com/BurntSushi/toml v0.4.1
	github.com/PuerkitoBio/goquery v1.7.1
	github.com/go-pg/pg/v9 v9.2.1
	github.com/gofiber/basicauth v0.2.2
	github.com/gofiber/compression v0.3.1
	github.com/gofiber/fiber v1.14.6
	github.com/gofiber/requestid v0.1.1
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/slack-go/slack v0.9.4
	github.com/ttgmpsn/go-snoonotes v0.1.2
	github.com/ttgmpsn/mira v0.1.13
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	golang.org/x/oauth2 v0.0.0-20210810183815-faf39c7919d5
)
