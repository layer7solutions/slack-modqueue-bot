package main

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/slack-go/slack/slackevents"
)

//lint:ignore U1000 tableName required for pg
type logSlackUser struct {
	tableName struct{} `pg:"slacklog_users"`
	UID       string   `pg:"type:char(11),pk"`
	Name      string   `pg:",notnull"`
}

//lint:ignore U1000 tableName required for pg
type logSlackChannel struct {
	tableName struct{} `pg:"slacklog_channels"`
	CID       string   `pg:"type:char(9),pk"`
	Name      string   `pg:",notnull"`
}

//lint:ignore U1000 tableName required for pg
type logSlackMessage struct {
	tableName struct{} `pg:"slacklog_messages"`
	ChannelID string   `pg:"type:char(9),notnull"`
	Channel   *logSlackChannel
	UserID    string `pg:"type:char(11),notnull"`
	User      *logSlackUser
	Time      time.Time `pg:",notnull"`
	Message   string    `pg:",notnull"`
}

//lint:ignore U1000 createSchema only used initially for debug.
func createSchema(db *pg.DB) error {
	for _, model := range []interface{}{(*logSlackChannel)(nil), (*logSlackUser)(nil), (*logSlackMessage)(nil)} {
		err := db.CreateTable(model, &orm.CreateTableOptions{
			FKConstraints: true,
		})
		if err != nil {
			fmt.Println(err)
		}
	}
	return nil
}

var logUsers = make(map[string]logSlackUser)
var logChannels = make(map[string]logSlackChannel)

//lint:ignore U1000 dbLogger used for debugging
type dbLogger struct{}

//lint:ignore U1000 dbLogger used for debugging
func (d dbLogger) BeforeQuery(c context.Context, q *pg.QueryEvent) (context.Context, error) {
	return c, nil
}

//lint:ignore U1000 dbLogger used for debugging
func (d dbLogger) AfterQuery(c context.Context, q *pg.QueryEvent) error {
	fmt.Println(q.FormattedQuery())
	return nil
}

func logChannelMessage(e *slackevents.MessageEvent) {
	l := log.WithField("prefix", "log").WithField("UID", e.User).WithField("CID", e.Channel)
	u, ok := logUsers[e.User]
	if !ok {
		l.Debug("new user")
		uI, err := slackAPI.GetUserInfo(e.User)
		if err != nil {
			l.WithError(err).Error("could not get user information")
		}
		u = logSlackUser{
			UID:  e.User,
			Name: uI.Name,
		}
		if err := db.Insert(&u); err != nil {
			pgErr, ok := err.(pg.Error)
			if !ok || !pgErr.IntegrityViolation() {
				l.WithError(err).Error("could not insert user to db")
				return
			}
		}
		logUsers[e.User] = u
	}
	c, ok := logChannels[e.Channel]
	if !ok {
		l.Debug("new channel")
		cI, err := slackAPI.GetConversationInfo(e.Channel, false)
		if err != nil {
			l.WithError(err).Error("could not get channel information")
		}
		c = logSlackChannel{
			CID:  e.Channel,
			Name: cI.Name,
		}
		if err := db.Insert(&c); err != nil {
			pgErr, ok := err.(pg.Error)
			if !ok || !pgErr.IntegrityViolation() {
				l.WithError(err).Error("could not insert channel to db")
				return
			}
		}
		logChannels[e.Channel] = c
	}
	t, err := strconv.ParseFloat(e.TimeStamp, 64)
	if err != nil {
		l.WithError(err).Error("could not parse timestamp")
		return
	}
	msg := logSlackMessage{
		ChannelID: c.CID,
		UserID:    u.UID,
		Time:      time.Unix(int64(t), 0),
		Message:   e.Text,
	}
	if err := db.Insert(&msg); err != nil {
		l.WithError(err).Error("could not insert message to db")
	}
}
