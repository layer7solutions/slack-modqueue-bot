package main

import (
	"encoding/json"
	"fmt"
	"net/url"
	"regexp"
	"strings"
	"time"
)

type toolboxRemovalConfig struct {
	PMsubject string          `json:"pmsubject"`
	LogReason string          `json:"logreason"`
	Header    string          `json:"header"`
	Footer    string          `json:"footer"`
	LogSub    string          `json:"logsub"`
	LogTitle  string          `json:"logtitle"`
	BanTitle  string          `json:"bantitle"`
	GetFrom   string          `json:"getfrom"`
	Reasons   []removalReason `json:"reasons"`
}

type removalReason struct {
	Text           string `json:"text"`
	FlairText      string `json:"flairText"`
	FlairCSS       string `json:"flairCSS"`
	Title          string `json:"title"`
	SubReasons     []string
	RemovePosts    bool
	RemoveComments bool
	ID             int
}

type toolboxConfig struct {
	Ver            int                  `json:"ver"`
	RemovalReasons toolboxRemovalConfig `json:"removalReasons"`
}

var toolboxConfCache struct {
	c *toolboxConfig
	t time.Time
}

// Replaces "{value}" in s with r[value]
func toolboxReplace(s string, r map[string]string) string {
	args, i := make([]string, len(r)*2), 0
	for k, v := range r {
		args[i] = "{" + k + "}"
		args[i+1] = v
		i += 2
	}
	return strings.NewReplacer(args...).Replace(s)
}

func getRemovalConfig(sub string) (*toolboxRemovalConfig, error) {
	l := log.WithField("prefix", "getRemovalConfig")

	if time.Now().Before(toolboxConfCache.t) {
		l.Debugf("got cached, still valid until %s", toolboxConfCache.t)
		return &toolboxConfCache.c.RemovalReasons, nil
	}

	l.Infof("Cache miss, getting new")
	data, err := reddit.Subreddit(sub).Wiki("toolbox")

	if err != nil {
		return nil, err
	}

	ret := &toolboxConfig{}
	if err = json.Unmarshal([]byte(data.ContentMD), ret); err != nil {
		return nil, err
	}

	retDecoded := toolboxRemovalConfig{
		PMsubject: ret.RemovalReasons.PMsubject,
		LogReason: ret.RemovalReasons.LogReason,
		// Header
		// Footer
		LogSub:   ret.RemovalReasons.LogSub,
		LogTitle: ret.RemovalReasons.LogTitle,
		BanTitle: ret.RemovalReasons.BanTitle,
		GetFrom:  ret.RemovalReasons.GetFrom,
		// Reasons
	}
	retDecoded.Header, _ = url.QueryUnescape(ret.RemovalReasons.Header)
	retDecoded.Footer, _ = url.QueryUnescape(ret.RemovalReasons.Footer)
	r1 := regexp.MustCompile(`<select(?:\s?id="[^"]+")>\s*((?:<option>(?:[^<]+)</option>\s*)+)</select>`)
	r2 := regexp.MustCompile(`<option>([^<]+)</option>`)

	for n, r := range ret.RemovalReasons.Reasons {
		reason, _ := url.QueryUnescape(r.Text)

		selects := r1.FindAllStringSubmatch(reason, -1)
		reasons := []string{}

		if len(selects) > 0 {
			// We only do the first dropdown to not make it too complicated. Only one case with two dropdowns anyways...
			texts := r1.Split(reason, -1)
			before := texts[0]
			after := strings.Join(texts[1:], " ")

			options := r2.FindAllStringSubmatch(selects[0][1], -1)
			for _, o := range options {
				reasons = append(reasons, strings.Replace(strings.Replace(strings.TrimSpace(fmt.Sprintf("%s %s %s", before, o[1], after)), "\n", "", -1), "\\", "", -1))
			}
		} else {
			reasons = []string{reason}
		}

		if !r.RemoveComments && !r.RemovePosts {
			// Legacy support: If both fields are missing (= false), it is for both kinds.
			r.RemoveComments = true
			r.RemovePosts = true
		}

		retDecoded.Reasons = append(retDecoded.Reasons, removalReason{
			Text:           reason,
			FlairText:      r.FlairText,
			FlairCSS:       r.FlairCSS,
			Title:          r.Title,
			SubReasons:     reasons,
			ID:             n,
			RemovePosts:    r.RemovePosts,
			RemoveComments: r.RemoveComments,
		})
	}

	ret.RemovalReasons = retDecoded

	toolboxConfCache.t = time.Now().Add(24 * time.Hour)
	toolboxConfCache.c = ret

	return &retDecoded, nil
}
